#!/bin/bash

python /srv/project/backend/manage.py collectstatic --noinput &
python /srv/project/backend/manage.py migrate --noinput &
uwsgi /srv/project/docker/uwsgi/uwsgi.ini
