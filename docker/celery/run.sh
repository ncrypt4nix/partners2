#!/bin/bash

set -e

cd /srv/project/backend
celery worker --app=core.celery -B -s /tmp/beat-schedule -E --loglevel=ERROR
