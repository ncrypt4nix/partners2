#!/bin/bash

set -e
cd /srv/project/backend
daphne -b 0.0.0.0 -p 8001 core.asgi:application
