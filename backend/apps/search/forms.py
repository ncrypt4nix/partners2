from haystack.forms import FacetedSearchForm
from apps.search.models import SearchHistory


class SearchFormCustomized(FacetedSearchForm):
    """Custom form using specific algorithms to firm"""
    def search(self):
        q = self.cleaned_data.get("q", None)
        if q:
            query_max_len = SearchHistory._meta.get_field('query').max_length
            SearchHistory.objects.update_or_create(
                query=q if len(q) <= query_max_len else q[:query_max_len]
            )
        return super(FacetedSearchForm, self).search()
