from django.urls import path
from apps.search.views import SearchViewCustomized


urlpatterns = [
    path('search/', SearchViewCustomized.as_view(), name='haystack_search'),
]
