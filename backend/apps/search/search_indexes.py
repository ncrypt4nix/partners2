from haystack import indexes
from apps.catalog.models import Product


class ProductIndex(indexes.ModelSearchIndex, indexes.Indexable):
    warehouses = indexes.FacetMultiValueField()

    class Meta:
        model = Product
        fields = ['art', 'warehouses']

    def prepare_warehouses(self, obj):
        """Prepare sections to faceted"""
        return [
            i.warehouse_name_id
            for i in obj.get_stock()
            if 0 < i.available_in_stock
        ]
