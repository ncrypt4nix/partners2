from django.http.response import HttpResponseRedirect
from django.views.generic import FormView
from haystack.generic_views import FacetedSearchView
from apps.catalog.models import Product
from apps.catalog.forms import ItemOrderForm
from apps.catalog.mixins import CatalogViewMixin
from apps.search.querysets import SearchQuerySetCustomized
from apps.search.forms import SearchFormCustomized


class SearchViewCustomized(FacetedSearchView, CatalogViewMixin):
    """Customized search view"""
    facet_fields = ['warehouses']
    queryset = SearchQuerySetCustomized()

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = self.gen_queryset(
            Product.objects.filter(
                id__in=[i.object.id for i in kwargs.get('object_list')]
            )
        )
        context = super().get_context_data(**kwargs)
        context['selected_warehouses'] = [
            int(i.split(':')[1])
            for i in self.request.GET.getlist('selected_facets')
        ]
        context['querystring'] = (
            '&'.join(
                ['q={}'.format(context.get('query'))] +
                [
                    'selected_facets=' + i
                    for i in self.request.GET.getlist('selected_facets')
                ]
            )
        )
        return context

    def get_form(self, form_class=None):
        method = self.request.method
        if method == 'GET':
            kwargs = FacetedSearchView.get_form_kwargs(self)
            if kwargs.get('data', {}).get('q'):
                kwargs['data'] = kwargs['data'].copy()
                kwargs['data']['q'] = kwargs['data']['q'].replace('-', ' ')
            return SearchFormCustomized(**kwargs)
        elif method == 'POST':
            return ItemOrderForm(**FormView.get_form_kwargs(self))
        return super().get_form(form_class)

    def form_valid(self, form):
        if self.request.method == 'POST':
            form.save(self.request, self.partner.id)
            return HttpResponseRedirect(self.get_success_url())
        return super().form_valid(form)
