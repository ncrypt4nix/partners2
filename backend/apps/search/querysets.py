from haystack.backends import SQ
from haystack.query import SearchQuerySet


class SearchQuerySetCustomized(SearchQuerySet):
    def auto_query(self, query_string, fieldname="content"):
        # adding SQ fixes a strange case-insensitive bug
        sqs = self.filter(
            SQ(**{fieldname + '__iexact': query_string}) |
            SQ(**{fieldname + '__exact': query_string})
        )
        if sqs.count():
            return sqs
        sqs = self.filter(
            SQ(**{fieldname + '__icontains': query_string}) |
            SQ(**{fieldname + '__contains': query_string})
        )
        return sqs
