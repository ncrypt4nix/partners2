from django.contrib import admin
from apps.search.models import SearchHistory


@admin.register(SearchHistory)
class SearchHistoryAdmin(admin.ModelAdmin):
    list_display = ('query', 'count', 'updated')
    readonly_fields = list_display
