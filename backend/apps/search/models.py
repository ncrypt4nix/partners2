from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext as _


class SearchHistory(models.Model):
    """History all search queries without repeat"""
    query = models.CharField(_('User Query'), max_length=128, unique=True)
    count = models.PositiveIntegerField(_('Count'), default=1)
    updated = models.DateTimeField(
        _('Last Query'),
        default=timezone.now
    )

    class Meta:
        verbose_name = _('Search History')
        verbose_name_plural = verbose_name

    def save(self, *args, **kwargs):
        if self.pk:
            self.count += 1
        super().save(*args, **kwargs)
