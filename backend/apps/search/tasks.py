from core.celery import app
from django.core import management


@app.task()
def rebuild_solr_index():
    management.call_command('rebuild_index', '--noinput')
