from django.urls import path
from apps.helpdesk.views import HelpDeskView, ChannelDetailView


urlpatterns = [
    path('helpdesk/', HelpDeskView.as_view(), name='channels_list'),
    path(
        'helpdesk/<str:uuid>/',
        ChannelDetailView.as_view(),
        name='channel_detail'
    )
]
