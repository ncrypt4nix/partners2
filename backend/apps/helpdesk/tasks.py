from webpush import send_user_notification
from pywebpush import WebPushException
from django.contrib.staticfiles.storage import staticfiles_storage
from django.conf import settings
from core.celery import app
from apps.helpdesk.models import Message
from django.contrib.auth.models import User


@app.task()
def send_chat_notification(msg_id):
    message = Message.objects.get(id=msg_id)
    payload = {
        "head": message.sender.first_name,
        "body": message.content,
        "icon": "{host}{url}".format(
            host=settings.BASE_HOST,
            url=staticfiles_storage.url('img/TE.png')
        ),
        "url": "{host}{url}".format(
            host=settings.BASE_HOST,
            url=message.channel.get_absolute_url()
        )
    }
    readers = message.channel.members.exclude(
        id=message.sender.id
    ).exclude(
        id__in=message.read.all()
    )
    users = User.objects.filter(
        id__in=readers
    ).values_list(
        'id', flat=True
    )
    for member in readers:
        if member not in users:
            message.channel.partner.users.remove(member)
            message.channel.type.supervisors.remove(member)
            continue
        try:
            send_user_notification(user=member, payload=payload, ttl=1000)
        except WebPushException:
            pass
