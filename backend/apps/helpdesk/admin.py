from django.contrib import admin

from apps.helpdesk.models import Channel, Message, TypeChannel


@admin.register(TypeChannel)
class TypeChannelAdmin(admin.ModelAdmin):
    pass


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('channel', 'sender', 'content', 'time')
    fields = ('channel', 'sender', 'content', 'time')
    readonly_fields = fields
