from django import forms

from apps.helpdesk.models import Message


class ChannelMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('sender', 'channel', 'content',)
        widgets = {
            'sender': forms.HiddenInput,
            'channel': forms.HiddenInput,
            'content': forms.Textarea(
                attrs={
                    'class': 'col-11',
                    'rows': '5',
                    'placeholder': 'Начните вводить сообщение'
                }
            )
        }
