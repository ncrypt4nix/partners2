import sys
import os
import base64
import datetime
from uuid import UUID
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.core.files.base import ContentFile
from django.template.defaultfilters import filesizeformat
from attachments.models import Attachment
from apps.helpdesk.models import Message, Channel
from apps.helpdesk.tasks import send_chat_notification
from apps.utils.exceptions import MaxSizeAttachException


FILE_UPLOAD_MAX_SIZE = getattr(
    settings,
    'FILE_UPLOAD_MAX_SIZE',
    5 * 1024 * 1024
)


class ChatConsumer(JsonWebsocketConsumer):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
            str(self.scope['url_route']['kwargs']['chat_room']),
            self.channel_name
        )
        self.scope['user'].attributes.online += 1
        self.scope['user'].attributes.save()
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            str(self.scope['url_route']['kwargs']['chat_room']),
            self.channel_name
        )
        self.scope['user'].attributes.online -= 1
        self.scope['user'].attributes.save()
        self.close()

    def receive_json(self, text_data):
        text = {'type': text_data['type']}
        user = self.scope['user']
        channel = Channel.objects.get(
            ident=UUID(int=self.scope['url_route']['kwargs']['chat_room'])
        )

        if text_data['type'] == 'create_message':
            obj = Message(
                sender=user,
                content=text_data['message'],
                channel=channel
            )
            today = datetime.date.today()
            date_now = datetime.datetime(
                year=today.year,
                month=today.month,
                day=today.day
            )
            attach = None
            try:
                if text_data.get('attach'):
                    file_attach = base64.b64decode(text_data['attach'])
                    if FILE_UPLOAD_MAX_SIZE < sys.getsizeof(file_attach):
                        raise MaxSizeAttachException
                    obj.save()
                    attach = Attachment(
                        creator=self.scope['user'],
                        content_type=ContentType.objects.get_for_model(obj),
                        object_id=obj.pk
                    )
                    filename = text_data.get('attachname', 'attach')
                    attach.attachment_file.save(
                        filename,
                        content=ContentFile(
                            file_attach,
                            name=filename
                        ),
                        save=True
                    )

                if not obj.id:
                    obj.save()
                if channel.messages.filter(time__gte=date_now).count() == 1:
                    obj.send_offline_message()
                text.update({
                    'id': obj.pk,
                    'message': obj.content,
                    'attach': {
                        'url': attach.attachment_file.url,
                        'name': os.path.basename(attach.attachment_file.name),
                        'size': filesizeformat(attach.attachment_file.size)
                    } if attach else None,
                    'sender': {
                        'name': obj.get_sender(),
                        'id': obj.sender.id,
                        'is_our': obj.sender.attributes.is_manager
                    },
                    'time': obj.formated_time,
                })
                send_chat_notification.delay(obj.id)
            except MaxSizeAttachException:
                text['type'] = 'error'
                text.update({
                    'error_message': (
                        'Превышен максимально допустимый размер файла'
                    )
                })

        elif text_data['type'] == 'read_message':
            text.update({
                'sender_id': user.id
            })

        elif text_data['type'] == 'remove_message':
            try:
                Message.objects.get(
                    sender=user,
                    channel=channel,
                    id=text_data['id'],
                    time__gt=Message.objects.filter(
                        channel=channel
                    ).exclude(
                        sender=user
                    ).latest('time').time,
                ).delete()
                text.update({
                    'id': text_data['id']
                })
            except Message.DoesNotExist:
                text['type'] = 'error'
                text.update({
                    'error_message': (
                        'Невозможно удалить сообщение'
                    )
                })

        elif text_data['type'] == 'type_message':
            sender = user.get_short_name()
            if user in channel.type.supervisors.all():
                sender = channel.type.manager.get_short_name()
            text.update({
                'message': text_data['message'],
                'sender': {'sender_id': user.id, 'sender': sender},
            })

        async_to_sync(self.channel_layer.group_send)(
            str(self.scope['url_route']['kwargs']['chat_room']),
            {
                "type": "chat.message",
                "text": text,
            },
        )

    def chat_message(self, event):
        self.send_json(event["text"])
