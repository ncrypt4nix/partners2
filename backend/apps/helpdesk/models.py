import uuid
import importlib
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.timezone import now
from django.http import Http404
from apps.trackemail.tasks import send_email


class TypeChannel(models.Model):
    title = models.CharField('Тип канала', max_length=100)
    supervisors = models.ManyToManyField(
        get_user_model(),
        related_name='supervisors',
        verbose_name='Наблюдатели',
        blank=True
    )
    manager = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    ident = models.CharField('Идентификатор', max_length=100, unique=True)
    internal = models.BooleanField('Внутренняя', default=False)

    ORDER_DISPUT = 'order_disput'
    SUPORT = 'support'
    REVERSE_TO_MODEL = {
        ORDER_DISPUT: 'apps.orders.models.Order',
    }
    SHOW_FIELD = {
        ORDER_DISPUT: 'number'
    }

    class Meta:
        verbose_name_plural = 'Типы каналов'
        verbose_name = 'Тип канала'

    def __str__(self):
        return str(self.title)


class Channel(models.Model):
    type = models.ForeignKey(TypeChannel, on_delete=models.CASCADE)
    ident = models.UUIDField(default=uuid.uuid4, editable=False)
    reverse_pk = models.PositiveIntegerField(blank=True, null=True)
    partner = models.ForeignKey('partner.Partner', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Канал'
        verbose_name_plural = 'Каналы'

    @classmethod
    def user_channels(cls, user):
        """get queryset all available channels to user"""
        return cls.objects.filter(
            Q(partner__users=user) |
            Q(type__supervisors=user)
        ).distinct()

    @property
    def members(self):
        """return all channel members, including type supervisors"""
        return get_user_model().objects.filter(
            id__in=[
                i.id for i in self.type.supervisors.all()
            ] + [
                i.id for i in self.partner.users.all()
            ]
        )

    def get_support_owner(self):
        return self.members.exclude(id__in=self.type.supervisors.all()).first()

    def read(self, user):
        for message in self.messages.all():
            if user == message.sender:
                continue
            if user not in message.read.all():
                message.read.add(user)

    @classmethod
    def get_support(cls, partner):
        support_type, created = TypeChannel.objects.get_or_create(
            ident=TypeChannel.SUPORT,
            defaults={
                'title': 'Тех.Поддержка'
            }
        )
        if created:
            support_type.supervisors.set(
                User.objects.filter(is_superuser=True)
            )
        return cls.objects.get_or_create(type=support_type, partner=partner)[0]

    def get_absolute_url(self):
        return reverse(
            'helpdesk:channel_detail',
            kwargs={'uuid': self.ident.int}
        )

    @property
    def reverse_object(self):
        try:
            return self._reverse_object
        except AttributeError:
            pass
        if not self.reverse_pk:
            return None
        path = TypeChannel.REVERSE_TO_MODEL[self.type.ident]
        module, model = path.rsplit('.', 1)
        try:
            self._reverse_object = getattr(
                importlib.import_module(module),
                model
            ).objects.get(pk=self.reverse_pk)
        except ObjectDoesNotExist:
            self.reverse_pk = None
            self.save()
            self._reverse_object = None
        return self._reverse_object

    def get_reverse_url(self):
        """reverse url about instance create this"""
        if not self.reverse_object:
            raise Http404
        return self.reverse_object.get_absolute_url()

    def get_reverse_name(self):
        if not self.reverse_object:
            return ''
        return getattr(
            self.reverse_object,
            TypeChannel.SHOW_FIELD[self.type.ident]
        ) or ''

    def __str__(self):
        return '{title} ({partner})'.format(
            title=self.type.title,
            partner=self.partner.short_name
        )


class Message(models.Model):
    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    channel = models.ForeignKey(
        Channel,
        on_delete=models.CASCADE,
        related_name='messages'
    )
    content = models.TextField('Тело сообщения')
    time = models.DateTimeField(default=now)
    read = models.ManyToManyField(
        get_user_model(),
        related_name='reads',
        blank=True
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def get_sender(self):
        sender = f'{self.sender.first_name} {self.sender.last_name}'
        if self.sender in self.channel.type.supervisors.all():
            return self.channel.type.manager.get_short_name()
        return sender

    def send_offline_message(self):
        recipients = self.channel.members.exclude(
            id=self.sender_id
        )
        for user in recipients:
            if user.attributes.is_online():
                continue
            send_email.delay(
                to=user.email,
                tpl_ident='new_message',
                username=user.get_short_name(),
                sender=self.get_sender(),
                channel=f'{self.channel}',
                url='http://{host}{url}'.format(
                    host=settings.ALLOWED_HOSTS[0],
                    url=self.channel.get_absolute_url()
                )
            )

    @staticmethod
    def format_date(date):
        return {
            'year': date.year,
            'month': date.month,
            'day': date.day,
            'hour': date.hour,
            'minute': date.minute,
            'second': date.second,
        }

    @property
    def formated_time(self):
        return self.__class__.format_date(self.time)

    def __str__(self):
        return f'{self.sender.email} --> {self.channel.__str__()}'
