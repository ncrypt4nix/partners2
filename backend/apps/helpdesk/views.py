import uuid
import os
import json

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Count, Subquery, OuterRef, Q
from django.shortcuts import redirect
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView, DetailView
from django.template.defaultfilters import filesizeformat
from attachments.models import Attachment

from apps.helpdesk.models import Channel, Message, TypeChannel
from apps.partner.views import PartnerViewMixin


class ContextChannels:
    def get_context_data(self, **kwargs):
        cnt = super().get_context_data(**kwargs)
        channels = Channel.user_channels(
            self.request.user
        ).annotate(
            unread_messages=Subquery(
                Message.objects.filter(
                    channel=OuterRef('pk'),
                ).exclude(
                    Q(read__in=[self.request.user]) |
                    Q(sender=self.request.user)
                ).values('channel').annotate(cnt=Count('pk')).values('cnt'),
                output_field=models.IntegerField()
            )
        ).prefetch_related('messages')
        try:
            first_date = Message.objects.earliest('time').time
        except Message.DoesNotExist:
            first_date = timezone.now()
        cnt['channels'] = sorted(
            channels,
            key=(
                lambda x:
                    x.messages.last().time
                    if x.messages.count()
                    else first_date
            ),
            reverse=True
        )
        return cnt


class HelpDeskView(
    ContextChannels,
    PartnerViewMixin,
    TemplateView
):
    template_name = 'helpdesk/channels_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for channel in context['channels']:
            if channel.type.ident == TypeChannel.SUPORT:
                return context
        context['channels'].append(Channel.get_support(self.partner))
        return context


class ChannelDetailView(ContextChannels, PartnerViewMixin, DetailView):
    template_name = 'helpdesk/channel_detail.html'

    def get_context_data(self, **kwargs):
        cnt = super().get_context_data(**kwargs)
        messages = self.get_object().messages.all().prefetch_related('sender')
        attachments = {
            i.object_id: i
            for i in Attachment.objects.filter(
                content_type=ContentType.objects.get_for_model(Message),
                object_id__in=messages
            )
        }
        cnt['msg_history'] = json.dumps(
            [
                {
                    'id': i.pk,
                    'message': i.content,
                    'attach': {
                        'url': attachments[i.id].attachment_file.url,
                        'name': os.path.basename(
                            attachments[i.id].attachment_file.name
                        ),
                        'size': filesizeformat(
                            attachments[i.id].attachment_file.size
                        )
                    } if attachments.get(i.id) else None,
                    'sender': {
                        'name': i.get_sender(),
                        'id': i.sender.id,
                        'is_our': i.sender.attributes.is_manager,
                    },
                    'time': Message.format_date(timezone.localtime(i.time)),
                }
                for i in messages
            ]
        )
        cnt['file_upload_max_size'] = settings.FILE_UPLOAD_MAX_SIZE
        try:
            cnt['last_accessible_date'] = Message.format_date(
                timezone.localtime(
                    Message.objects.filter(
                        channel=self.get_object()
                    ).exclude(
                        sender=self.request.user
                    ).latest('time').time
                )
            )
        except Message.DoesNotExist:
            cnt['last_accessible_date'] = Message.format_date(timezone.now())
        return cnt

    def get_queryset(self):
        return Channel.objects.user_channels(self.request.user)

    def get(self, request, *args, **kwargs):
        uuid_int = self.kwargs.get('uuid')
        if not uuid_int.isdigit():
            return redirect(reverse('helpdesk:channels_list'))
        self.get_object().read(self.request.user)
        return super().get(request, *args, **kwargs)

    def get_object(self):
        uuid_int = self.kwargs.get('uuid')
        try:
            return Channel.objects.prefetch_related(
                'messages',
                'messages__sender'
            ).get(
                ident=uuid.UUID(int=int(uuid_int))
            )
        except (ObjectDoesNotExist, ValueError):
            return redirect(reverse('helpdesk:channels_list'))
