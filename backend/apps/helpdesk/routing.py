from django.urls import path
from apps.helpdesk.consumers import ChatConsumer


websocket_urlpatterns = [
    path('ws/chat/<int:chat_room>/', ChatConsumer),
]
