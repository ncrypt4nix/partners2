from django.db.models import Q, Sum, Prefetch
from django.views.generic import FormView
from apps.partner.views import PartnerViewMixin
from apps.catalog.models import ProductPrice
from apps.catalog.forms import ItemOrderForm


class CatalogViewMixin(PartnerViewMixin, FormView):
    form_class = ItemOrderForm
    paginate_by = 24

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        querystring = self.request.GET.copy()
        querystring.pop('page', None)
        context['querystring'] = querystring.urlencode()
        context['catalog_last_sync'] = self.partner.synclog_set.filter(
            type='catalog'
        ).first()
        context['warehouses'] = self.partner.warehouses.filter(
            items__available_in_stock__gt=0
        ).distinct()
        context['selected_warehouses'] = [
            int(i)
            for i in self.request.GET.getlist('stock_name')
        ]
        favorites = self.request.user.favoriteproduct_set.all()
        context['favorites'] = favorites.values_list(
            'products__art',
            flat=True
        )
        context['object_list'] = context['object_list'].annotate(
            warehouse=Sum(
                'stock__available_in_stock',
                filter=Q(
                    stock__warehouse_name__in=(
                        context['selected_warehouses']
                        if context['selected_warehouses']
                        else self.partner.get_all_warehouses()
                    )
                )
            ),
            to_warehouse=Sum(
                'stock__available_in_transit',
                filter=Q(
                    stock__warehouse_name__in=(
                        context['selected_warehouses']
                        if context['selected_warehouses']
                        else self.partner.get_all_warehouses()
                    )
                )
            )
        )
        return context

    def gen_queryset(self, qs):
        return qs.prefetch_related(
            'stock',
            'section',
            Prefetch(
                'productprice_set',
                queryset=ProductPrice.objects.filter(type='eu'),
                to_attr='eu_price'
            )
        ).order_by('section', 'position').distinct()

    def form_valid(self, form):
        form.save(self.request, self.partner)
        return super().form_valid(form)

    def get_success_url(self):
        return self.request.get_full_path()
