import django_filters
from django import forms
from apps.catalog.models import Product, Section, WarehouseItem


def sections(request):
    if request is None:
        return Section.objects.none()
    return Section.objects.get(
        code_1c=request.resolver_match.kwargs['vendor']
    ).get_descendants()


class ProductFilter(django_filters.FilterSet):
    section = django_filters.ModelMultipleChoiceFilter(
        widget=forms.CheckboxSelectMultiple,
        label='Секции',
        queryset=sections,
        method='custom_sections'
    )
    stock_name = django_filters.ModelMultipleChoiceFilter(
        field_name='stock__warehouse_name',
        to_field_name="warehouse_name_id",
        queryset=WarehouseItem.objects.filter(available_in_stock__gt=0),
        method='stock_filter'
    )

    class Meta:
        model = Product
        fields = ['section', 'stock_name']

    def stock_filter(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(stock__in=value)

    def custom_sections(self, queryset, name, value):
        if not value:
            return queryset
        suffix = '__in'
        new_value = [
            x
            for y in value
            for x in y.get_descendants(include_self=True)
        ]
        return queryset.filter(**{
            name + suffix: new_value,
        })
