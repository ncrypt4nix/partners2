import datetime
import requests
from zeep import Client
from django.conf import settings
from apps.catalog.models import (
    Product,
    ProductPrice,
    WarehouseItem,
    Warehouse,
    PartnerProductPrice,
    PartnerProductLimitPrice,
    PartnerVendorLimitPrice,
    Vendor,
    Section,
    Currency
)
from apps.partner.models import Partner
from core.celery import app
from core.soap_interface import SoapInterface, ConnectionError


class MatrixDict:
    def __init__(self):
        self.__items = dict()

    def release(self):
        return [
            item
            for i in self.__items.values()
            for item in i.values()
        ]

    def get_native(self):
        return self.__items

    def get_item(self, i, j):
        return self.__items.get(i, {}).get(j)

    def pop_item(self, i, j):
        try:
            item = self.__items.get(i, {}).pop(j)
            if not self.__items.get(i):
                self.__items.pop(i)
            return item
        except KeyError:
            return None

    def set_item(self, i, j, item):
        try:
            self.__items[i].update({j: item})
        except KeyError:
            self.__items[i] = {j: item}


@app.task(ignore_result=True)
def sync_currency():
    client = Client('http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL')
    response = client.service.GetCursOnDate(datetime.datetime.now())
    currency = Currency.objects.latest('date')
    for item in response['_value_1']['_value_1']:
        curs = item['ValuteCursOnDate']
        setattr(currency, curs['VchCode'].lower(), curs['Vcurs'])
    currency.save()


@app.task(ignore_result=True)
def sync_nomenclatures_with_price():
    connection_1c = SoapInterface()
    try:
        res = connection_1c.get_nomenclatures()
    except ConnectionError:
        return
    if res['Error']['Flag']:
        return
    sections = {i.code_1c: i for i in Section.objects.all()}
    new_sections = list()
    update_sections = dict()
    real_update_sections = dict()
    new_related_sections = dict()
    new_product_sections = dict()
    products = {i.code_1c: i for i in Product.objects.all()}
    new_products = list()
    update_products = dict()
    real_update_products = dict()
    vendors = {i.ident: i for i in Vendor.objects.all()}
    prices = MatrixDict()
    update_prices = MatrixDict()
    real_update_prices = MatrixDict()
    new_prices = MatrixDict()
    absolute_new_prices = MatrixDict()
    for price in ProductPrice.objects.all():
        prices.set_item(price.product.code_1c, price.type, price)
    for item in res['Nomenclatures']:
        if item['IsGroup']:
            section_defaults = {
                'title': item['FullName'],
                'published': True,
                'position': item['Ordering'],
                'parent': (
                    sections.get(item['ParentCode']) or
                    update_sections.get(item['ParentCode']) or
                    real_update_sections.get(item['ParentCode'])
                )
            }
            if sections.get(item['Code']):
                section = sections.pop(item['Code'])
                update_flag = False
                for key, value in section_defaults.items():
                    if getattr(section, key) == value:
                        continue
                    setattr(section, key, value)
                    update_flag = True
                if update_flag:
                    real_update_sections.update({item['Code']: section})
                else:
                    update_sections.update({item['Code']: section})
            else:
                section = Section(
                    code_1c=item['Code'],
                    **section_defaults,
                    lft=1,
                    rght=1,
                    tree_id=1,
                    level=1
                )
                new_sections.append(section)
            if not section_defaults['parent'] and item['ParentCode']:
                try:
                    new_related_sections[item['ParentCode']].append(
                        section.code_1c
                    )
                except KeyError:
                    new_related_sections[item['ParentCode']] = [
                        section.code_1c
                    ]
            continue
        vendor = vendors.get(item['Brand'].lower())
        if not vendor:
            vendor = Vendor.objects.create(ident=item['Brand'].lower())
            vendors.update({vendor.ident: vendor})
        product_defaults = {
            'art': item['Article'],
            'title': item['FullName'],
            'vendor': vendor,
            'position': item['Ordering'],
            'url': item['WebRef'],
            'published': True,
            'section': (
                sections.get(item['ParentCode']) or
                update_sections.get(item['ParentCode']) or
                real_update_sections.get(item['ParentCode'])
            )
        }
        if products.get(item['Code']):
            product = products.pop(item['Code'])
            update_flag = False
            for key, value in product_defaults.items():
                if getattr(product, key) == value:
                    continue
                setattr(product, key, value)
                update_flag = True
            if update_flag:
                real_update_products.update({item['Code']: product})
            else:
                update_products.update({item['Code']: product})
        else:
            product = Product(code_1c=item['Code'], **product_defaults)
            new_products.append(product)
        if not product_defaults['section'] and item['ParentCode']:
            new_product_sections[product.code_1c] = item['ParentCode']
        for price in item['Prices']:
            price_defaults = {
                'type': price['Name'].lower(),
                'value': price['Value'],
                'currency': price['Currency'],
                'unit': price['Unit'],
            }
            price = prices.pop_item(product.code_1c, price_defaults['type'])
            if price:
                update_flag = False
                for key, value in price_defaults.items():
                    if getattr(price, key) == value:
                        continue
                    setattr(price, key, value)
                    update_flag = True
                if update_flag:
                    real_update_prices.set_item(
                        product.code_1c,
                        price_defaults['type'],
                        price
                    )
                else:
                    update_prices.set_item(
                        product.code_1c,
                        price_defaults['type'],
                        price
                    )
            else:
                price = ProductPrice(product=product, **price_defaults)
                if (not update_products.get(product.code_1c) and not
                        real_update_products.get(product.code_1c)):
                    absolute_new_prices.set_item(
                        product.code_1c,
                        price_defaults['type'],
                        price
                    )
                    continue
                new_prices.set_item(
                    product.code_1c,
                    price_defaults['type'],
                    price
                )

    new_sections = {
        i.code_1c: i
        for i in Section.objects.bulk_create(new_sections)
    }
    new_products = {
        i.code_1c: i
        for i in Product.objects.bulk_create(new_products)
    }
    # add created product to prices without products
    for product_code, price_types in absolute_new_prices.get_native().items():
        product = new_products.get(product_code)
        for next_price in price_types.values():
            next_price.product = product
            new_prices.set_item(product.code_1c, next_price.type, next_price)
    ProductPrice.objects.bulk_create(new_prices.release())
    # update section tree
    for parent_code, child_codes in new_related_sections.items():
        parent = (
            new_sections.get(parent_code) or
            update_sections.get(parent_code) or
            real_update_sections.get(parent_code)
        )
        for child_code in child_codes:
            real_update_sections[child_code] = (
                update_sections.get(child_code) or
                real_update_sections.get(child_code) or
                new_sections.get(child_code)
            )
            real_update_sections[child_code].parent = parent
    # update products sections
    for product_code, section_code in new_product_sections.items():
        section = (
            new_sections.get(section_code) or
            update_sections.get(section_code) or
            real_update_sections.get(section_code)
        )
        real_update_products[product_code] = (
            new_products.get(product_code) or
            update_products.get(product_code) or
            real_update_products.get(product_code)
        )
        real_update_products[product_code].section = section
    Section.objects.bulk_update(
        [*real_update_sections.values()],
        [*section_defaults.keys()]
    )
    Product.objects.bulk_update(
        [*real_update_products.values()],
        [*product_defaults.keys()]
    )
    ProductPrice.objects.bulk_update(
        real_update_prices.release(),
        [*price_defaults.keys()]
    )
    Section.objects.filter(id__in=[i.id for i in sections.values()]).delete()
    Product.objects.filter(id__in=[i.id for i in products.values()]).delete()
    ProductPrice.objects.filter(
        id__in=[i.id for i in prices.release()]
    ).delete()
    Section.objects.rebuild()


@app.task()
def sync_warehouse_for_products():
    try:
        data = SoapInterface().get_warehouses()
    except ConnectionError:
        return
    if data['Error']['Flag']:
        return
    products = {
        i.code_1c: i
        for i in Product.objects.all()
    }
    warehouses = {
        i.name: i
        for i in Warehouse.objects.all()
    }
    warehouse_items = MatrixDict()
    real_update_warehouse_items = list()
    create_warehouse_items = list()
    for item in WarehouseItem.objects.all().select_related(
            'product', 'warehouse_name'):
        warehouse_items.set_item(
            item.product.code_1c,
            item.warehouse_name.name,
            item
        )
    for item in data['Availability']:
        if not products.get(item['Code']):
            continue
        warehouse = warehouses.get(item['WarehouseName'])
        if not warehouse:
            warehouse = Warehouse.objects.create(name=item['WarehouseName'])
            warehouses[item['WarehouseName']] = warehouse
        defaults = {
            'product': products[item['Code']],
            'warehouse_name': warehouse,
            'available_in_stock': item['InStock'],
            'available_in_transit': item['InTransit'],
        }
        warehouse_item = warehouse_items.pop_item(
            item['Code'],
            item['WarehouseName']
        )
        if not warehouse_item:
            create_warehouse_items.append(WarehouseItem(**defaults))
            continue
        update_flug = False
        for key, value in defaults.items():
            if getattr(warehouse_item, key) == value:
                continue
            setattr(warehouse_item, key, value)
            update_flug = True
        if update_flug:
            real_update_warehouse_items.append(warehouse_item)
    WarehouseItem.objects.bulk_create(create_warehouse_items)
    WarehouseItem.objects.bulk_update(
        real_update_warehouse_items,
        [*defaults.keys()]
    )
    WarehouseItem.objects.filter(
        id__in=[i.id for i in warehouse_items.release()]
    ).delete()
