from django.core.exceptions import FieldError
from django.db.models import Q, Sum
from django.views.generic import TemplateView, UpdateView, ListView
from django_filters.views import FilterView
from rest_framework import viewsets
from rest_framework.response import Response

from apps.catalog.filters import ProductFilter
from apps.catalog.forms import OrderForm, OrderFormSet
from apps.catalog.models import Section, Product, FavoriteProduct
from apps.catalog.mixins import CatalogViewMixin
from apps.orders.models import ItemOrder
from apps.partner.views import PartnerViewMixin


class CatalogView(PartnerViewMixin, TemplateView):
    template_name = 'catalog/catalog.html'

    def get_context_data(self, **kwargs):
        cnt = super(CatalogView, self).get_context_data(**kwargs)
        cnt['vendors'] = Section.objects.root_nodes()
        return cnt


class VendorProductsView(FilterView, CatalogViewMixin):
    template_name = 'catalog/products.html'
    filterset_class = ProductFilter

    def get_queryset(self):
        sections = Section.objects.get(
            code_1c=self.kwargs['vendor']
        ).get_descendants(include_self=True)
        qs = self.gen_queryset(
            Product.objects.filter(
                section__in=sections
            )
        )
        if 'sort' in self.request.GET:
            try:
                sorted_qs = qs.order_by(
                    self.request.GET['sort']
                )
                # execute queryset to verify exist field for order_by
                sorted_qs.first()
                return sorted_qs
            except FieldError:
                pass
        return qs

    def get_context_data(self, **kwargs):
        cnt = super().get_context_data(**kwargs)
        cnt['tpl_sort'] = True
        cnt['vendors'] = Section.objects.root_nodes()
        cnt['section'] = Section.objects.get(code_1c=self.kwargs['vendor'])
        cnt['section_tree'] = cnt['section'].get_descendants()
        cnt['sections'] = [
            int(x)
            for x in self.request.GET.getlist('section')
        ]
        return cnt


class CatalogCartView(PartnerViewMixin, UpdateView):
    template_name = 'catalog/cart.html'
    form_class = OrderForm

    def get_object(self, queryset=None):
        return self.request.user.attributes.cart(self.partner)

    def get_context_data(self, **kwargs):
        cnt = super(CatalogCartView, self).get_context_data(**kwargs)
        if self.request.POST:
            cnt['formset'] = OrderFormSet(
                self.request.POST,
                instance=self.get_object()
            )
        else:
            all_warehouses = self.partner.get_all_warehouses()
            cnt['formset'] = OrderFormSet(
                instance=self.get_object(),
                queryset=ItemOrder.objects.annotate(
                    warehouse=Sum(
                        'product__stock__available_in_stock',
                        filter=Q(
                            product__stock__warehouse_name__in=all_warehouses
                        )
                    )
                ).select_related('product')
            )
        return cnt


class FavoriteProductViewSet(PartnerViewMixin, viewsets.ViewSet):
    """Ajax to add or rm products from partner"""
    def get_queryset(self):
        qs, _ = FavoriteProduct.objects.get_or_create(user=self.request.user)
        return qs

    def update(self, request):
        product = Product.objects.get(art=request.data.get('art'))
        qs = self.get_queryset()
        if product not in qs.products.all():
            qs.products.add(product)
        else:
            qs.products.remove(product)
        return Response(' ')


class FavoriteProductView(ListView, CatalogViewMixin):
    """View to favorite page"""
    template_name = "catalog/favorite.html"

    def get_queryset(self):
        qs = Product.objects.none()
        try:
            qs = FavoriteProduct.objects.get(
                user=self.request.user
            ).products.all()
        except FavoriteProduct.DoesNotExist:
            pass
        return self.gen_queryset(qs)
