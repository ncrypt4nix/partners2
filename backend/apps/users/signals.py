from django.dispatch import receiver
from django_cas_ng.signals import cas_user_authenticated

from apps.users.models import Attributes


@receiver(cas_user_authenticated)
def set_company(user, attributes, **kwargs):
    Attributes.objects.update_or_create(
        user=user,
        defaults={'data': attributes}
    )
