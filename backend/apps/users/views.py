from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from apps.users.forms import ManagerForm, PartnerSettings
from apps.partner.views import PartnerViewMixin


class ProfileSettings(PartnerViewMixin, FormView):
    """settings page to partner or manager"""
    template_name = 'users/settings.html'
    success_url = reverse_lazy('users:settings')

    def get_form(self, form_class=None):
        kwargs = super().get_form_kwargs()
        if self.is_employee:
            return ManagerForm(
                instance=self.request.user.attributes,
                **kwargs
            )
        else:
            return PartnerSettings(
                instance=self.request.user.attributes,
                **kwargs
            )

    def form_valid(self, form):
        form.save(self.partner)
        return super().form_valid(form)
