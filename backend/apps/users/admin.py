from django.contrib import admin
from apps.users.models import Attributes


@admin.register(Attributes)
class AttributesAdmin(admin.ModelAdmin):
    pass
