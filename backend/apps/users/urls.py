from django.urls import path
from apps.users.views import ProfileSettings


urlpatterns = [
    path('account', ProfileSettings.as_view(), name="settings")
]
