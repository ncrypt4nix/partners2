from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from attachments.models import Attachment
from django.contrib.postgres.fields import JSONField, ArrayField
from apps.orders.models import Order
from django.db import models


class DisplayUser(User):
    class Meta:
        proxy = True

    def __str__(self):
        return f'{self.last_name} {self.first_name}'


class Attributes(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    data = JSONField()
    phone = models.CharField('Телефон', max_length=50, blank=True, null=True)
    mobile = models.CharField(
        'Мобильный Телефон',
        max_length=50,
        blank=True,
        null=True
    )
    skype = models.CharField('Skype', max_length=50, blank=True, null=True)
    telegram = models.CharField(
        'Telegram',
        max_length=50,
        blank=True,
        null=True
    )
    online = models.IntegerField(default=0)
    email_me_statuses = ArrayField(
        models.CharField(
            max_length=2,
            choices=Order.STATUS,
        ),
        verbose_name="Отправка email уведомлений при изменении статуса заказа",
        default=list,
        blank=True,
    )

    def is_online(self):
        return bool(self.online)

    def __str__(self):
        return str(self.user.email)

    @property
    def is_manager(self):
        """return True if user is manager"""
        try:
            return 50 <= int(self.data.get('access_level', 0))
        except ValueError:
            return False

    def cart(self, partner):
        """Cart to current user and current partner"""
        return Order.objects.get_or_create(
            responsible=self.user,
            status=Order.Statuses.TRASH,
            partner=partner
        )[0]

    def save(self, **kwargs):
        self.user.user_permissions.add(
            *Permission.objects.filter(
                content_type=ContentType.objects.get_for_model(Attachment),
                codename__in=['add_attachment', 'delete_attachment']
            )
        )
        super().save(**kwargs)
