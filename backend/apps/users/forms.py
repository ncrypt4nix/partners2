from django import forms
from apps.users.models import Attributes
from apps.orders.models import Order
from apps.utils.logger import write_log


class PartnerSettings(forms.ModelForm):
    """choice email to change status"""
    email_me_statuses = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=[
            i
            for i in Attributes._meta.get_field(
                'email_me_statuses'
            ).base_field.choices
            if (i[0] not in Order.Statuses.LOCAL_STATUSES or
                i[0] == Order.Statuses.CANCELED)
        ],
        label="Отправка email уведомлений, при изменении статуса заказа",
        required=False
    )

    class Meta:
        model = Attributes
        fields = ('email_me_statuses',)

    def save(self, partner):
        obj = Attributes.objects.get(user=self.instance.user)
        for key, value in self.cleaned_data.items():
            old_value = getattr(obj, key)
            if old_value == value:
                continue
            write_log(
                self.instance.user,
                partner,
                'User changed field {field}: '
                'from {from_value} to {to_value}'.format(
                    field=key,
                    from_value=old_value,
                    to_value=value
                )
            )
        super().save()


class ManagerForm(PartnerSettings):
    """form for settings page"""
    class Meta(PartnerSettings.Meta):
        fields = (
            ('phone', 'mobile', 'skype', 'telegram') +
            PartnerSettings.Meta.fields
        )
