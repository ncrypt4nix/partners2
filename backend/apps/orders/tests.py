from abc import ABC
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from core.utils import test_partner_view
from apps.orders.models import Order, ItemOrder
from apps.partner.models import Partner
from apps.catalog.models import Product


class OrderPrepairInterface(ABC):
    fixtures = [
        'apps/partner/fixtures/partner.json',
        'apps/catalog/fixtures/catalog_vendors.json',
        'apps/catalog/fixtures/catalog_sections.json',
        'apps/catalog/fixtures/catalog_products.json',
    ]

    def setUp(self):
        self.o1 = Order(
            partner=Partner.objects.first(),
            responsible=User.objects.first(),
            status=Order.Statuses.SHIPPED,
            order_id='SD1212'
        )
        self.o1.save()
        products = Product.objects.all()[:2]
        self.items = [
            ItemOrder(
                order=self.o1,
                art=products[0].art,
                product=products[0],
                shipped=12.3,
                price=999.00,
                real_price=999.00,
                discount=15,
            ),
            ItemOrder(
                order=self.o1,
                art=products[1].art,
                shipped=12.3,
                price=999.00,
                real_price=999.00,
                discount=15,
            )
        ]
        ItemOrder.objects.bulk_create(self.items)


class TestOrderModel(OrderPrepairInterface, TestCase):
    """testing methods to model Order"""
    def test_repeat_order(self):
        o2 = self.o1.repeat_order(responsible=User.objects.last())
        self.assertIsInstance(o2, Order)

        self.assertNotEqual(self.o1.pk, o2.pk)
        self.assertNotEqual(self.o1.responsible, o2.responsible)
        self.assertIsNone(o2.order_id)
        self.assertEqual(o2.status, Order.Statuses.DRAFT)

        items = o2.items.all()
        self.assertNotEqual(len(items), 0)
        for item in items:
            self.assertNotIn(item.pk, [i.pk for i in self.items])
            self.assertIn(item.art, [i.art for i in self.items])

            self.assertIsNone(item.discount)
            self.assertIsNone(item.shipped)
            self.assertIsNone(item.in_stock)
            self.assertIsNone(item.awaiting)
            self.assertIsNone(item.awaiting_date)
            self.assertIsNone(item.price)
            self.assertIsNone(item.real_price)
            self.assertIsNone(item.amount)


@test_partner_view
class TestOrderDetailView(OrderPrepairInterface, TestCase):
    """testing order-detail view"""
    def setUp(self):
        super().setUp()
        self.url = reverse('orders:detail', args=(self.o1.pk,))

    def test_repeat_order_action(self):
        response = self.client.post(
            self.url,
            {'action': 'repeat_order'},
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['object'].status, Order.Statuses.DRAFT)
