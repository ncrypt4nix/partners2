from django.utils.translation import ugettext as _


class FileError(Exception):
    """Base exception to all errors occurs when work with file"""
    _msg = _(
        'Файл должен быть в формате xlsx и содержать:'
        'первым столбцом артикул, вторым кол-во.'
    )

    def __init__(self, num, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msg = self._msg + _('Строка: ') + str(num)

    def __str__(self):
        return self.msg


class WrongFileFormat(FileError):
    """wrong file format"""
    _msg = _('Неверный формат файла.')


class RequiredProductDoesNotExis(FileError):
    """occurs when if one and more product does not exist in db"""
    _msg = _('Продукт не найден.')


class EmptyFileError(FileError):
    """file with zero length"""
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


class FileErrorList(FileError):
    """exception about all exceptions in file"""
    BREAK_SYMBOL = '<br />'

    def __init__(self, error_list, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)
        self.msg = self._msg + self.BREAK_SYMBOL
        for error in error_list:
            self.msg += error.msg + self.BREAK_SYMBOL
