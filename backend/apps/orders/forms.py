from django import forms
from django.utils.translation import ugettext_lazy as _


class UploadOrderExcel(forms.Form):
    """form to create order from xlsx file"""
    xlsx_file = forms.FileField()
    XLSX_FORMAT = (
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )

    def clean(self):
        file = super().clean().get('xlsx_file')
        if file.content_type != self.XLSX_FORMAT:
            raise forms.ValidationError(_('Not xlsx file'))
