import datetime
from django.conf import settings
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.db.models import Sum, Q
from django.db.models.functions import Coalesce
from django.http import FileResponse, HttpResponseRedirect
from django.views.generic import DetailView
from django_filters.views import FilterView

from apps.catalog.forms import OrderFormSet
from apps.orders.filters import OrderFilter
from apps.orders.forms import UploadOrderExcel
from apps.orders.models import ItemOrder, Order
from apps.partner.views import PartnerViewMixin
from apps.utils.logger import write_log
from apps.orders.exceptions import FileError, FileErrorList, EmptyFileError
from apps.trackemail.tasks import send_email

from core.soap_interface import ConnectionError


class OrdersListView(PartnerViewMixin, FilterView):
    template_name = 'orders/orders.html'
    context_object_name = 'orders'
    paginate_by = 75
    filterset_class = OrderFilter

    def get_queryset(self):
        return self.partner.orders.exclude(
            status__in=Order.Statuses.TRASH
        ).exclude(
            Q(status__in=Order.Statuses.DRAFT)
            & ~Q(responsible=self.request.user)
        ).order_by(
            Coalesce(
                'created_1c',
                'created'
            ).desc()
        )

    def get(self, request, not_sync=False, *args, **kwargs):
        if not_sync:
            return super().get(request, *args, **kwargs)
        try:
            page = int(request.GET.get('page', 1))
        except TypeError:
            page = 1
        qs = self.paginator_class(
            self.get_queryset(),
            self.paginate_by
        ).page(page or 1).object_list
        try:
            if not qs:
                self.partner.update_orders_list()
            elif page != 1:
                self.partner.update_orders_list(order_qs=qs)
            else:
                date_from = qs[len(qs) - 1].date
                date_to = datetime.date.today()
                self.partner.update_orders_list(date_from, date_to)
        except ConnectionError:
            pass
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.POST.get('action') == 'sync':
            return self.get(request, *args, **kwargs)
        if request.POST.get('action') == 'load_xlsx':
            form = UploadOrderExcel(request.POST, request.FILES)
            if not form.is_valid():
                self.kwargs['xlsx_error'] = FileError._msg
                return self.get(request, not_sync=True, *args, **kwargs)
            try:
                new_order = Order.from_excel(
                    file=form.cleaned_data['xlsx_file'].file,
                    partner=self.partner
                )
            except FileErrorList as error:
                self.kwargs['xlsx_error'] = error.msg
                return self.get(request, not_sync=True, *args, **kwargs)
            except EmptyFileError:
                self.kwargs['xlsx_error'] = EmptyFileError._msg
                return self.get(request, not_sync=True, *args, **kwargs)
            return HttpResponseRedirect(
                reverse('orders:detail', args=[new_order.pk])
            )
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['xlsx_form'] = UploadOrderExcel()
        context['xlsx_error'] = self.kwargs.get('xlsx_error')
        return context


class OrderDetailView(PartnerViewMixin, DetailView):
    template_name = 'orders/order_detail.html'

    def dispatch(self, request, *args, **kwargs):
        origin = super().dispatch(request, *args, **kwargs)
        if 'action' in request.POST:
            action = request.POST.get('action')
            if action == 'close':
                obj = self.get_object()
                obj.status = Order.Statuses.CANCELED
                obj.save(update_fields=['status'])
                write_log(
                    request.user,
                    self.partner,
                    'User close order with id: {id}'.format(id=obj.id)
                )
            elif action == 'to_trash':
                self.partner.orders.filter(
                    status=Order.Statuses.TRASH
                ).update(status=Order.Statuses.DRAFT)
                obj = self.get_object()
                obj.status = Order.Statuses.TRASH
                obj.save()
                write_log(
                    request.user,
                    self.partner,
                    'User back order to trash with id: {id}'.format(id=obj.id)
                )
                return redirect(reverse('catalog:cart'))
            elif action == 'repeat_order':
                return redirect(
                    self.get_object().repeat_order(
                        responsible=request.user
                    ).get_absolute_url()
                )
            elif action == 'to_disput':
                return redirect(self.get_object().create_disput(request.user))
            elif action == 'send':
                obj = self.get_object()
                obj.status = Order.Statuses.SEND
                obj.save()
                send_email.delay(
                    [user.email for user in obj.partner.get_only_managers()],
                    'local_create_order',
                    username=obj.partner.manager.get_short_name(),
                    partner='{short_name} ({contragentcod})'.format(
                        short_name=obj.partner.short_name,
                        contragentcod=obj.partner.contragentcod
                    ),
                    url='http://{host}{url}'.format(
                        host=settings.ALLOWED_HOSTS[0],
                        url=obj.get_absolute_url()
                    )
                )
                write_log(
                    request.user,
                    self.partner,
                    'User create order with id: {id}'.format(id=obj.id)
                )
            return redirect(reverse('orders:list'))
        return origin

    def get_queryset(self):
        return self.partner.orders

    def post(self, request, *args, **kwargs):
        if self.request.POST:
            formset = OrderFormSet(
                self.request.POST,
                instance=self.get_object()
            )
            try:
                if formset.is_valid():
                    formset.save()
            except ValidationError:
                pass
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        cnt = super().get_context_data(**kwargs)
        warehouses = self.partner.get_all_warehouses()
        cnt['formset'] = OrderFormSet(
            instance=self.get_object(),
            queryset=ItemOrder.objects.annotate(
                warehouse=Sum(
                    'product__stock__available_in_stock',
                    filter=Q(
                        product__stock__warehouse_name__in=warehouses
                    )
                )
            )
        )
        return cnt

    @staticmethod
    def get_excel(request, pk):
        """generate and response order excel file"""
        file = get_object_or_404(Order, pk=pk).to_excel()
        return FileResponse(
            open(file.name, 'rb'),
            as_attachment=True,
            filename='order_sheet.xlsx'
        )
