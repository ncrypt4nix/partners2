import datetime
import django.dispatch
from django.dispatch import receiver
from core.soap_interface import ConnectionError

order_sync = django.dispatch.Signal(providing_args=['instance'])


@receiver(order_sync)
def sync(sender, instance, **kwargs):
    from apps.orders.models import ItemOrder, Order

    if instance.status == Order.Statuses.CLOSED:
        return
    try:
        res = instance.order_info()
    except ConnectionError:
        instance.log = f'{ConnectionError}'
        instance.save()
        return
    if res['Error']['Flag']:
        instance.log = res['Error']['Description']
        instance.save()
        return
    instance.total = res['Total']
    instance.currency = res['Currency']
    instance.created_1c = res['Date']
    instance.number = res['Number']
    instance.warehouse = res['Warehouse']
    instance.comments = res['PartnerComment']
    instance.last_sync = datetime.datetime.now()
    shipped_count = 0.
    in_stock_count = 0.
    quantity_count = 0.
    awaiting_count = 0.
    delete_idx = {item.art: item for item in instance.items.all()}
    update_items = []
    created_items = []
    for soap_item in res['Items']:
        update_fields = {
            'art': soap_item['Article'],
            'quantity': soap_item['Quantity'],
            'price': soap_item['Price'],
            'real_price': soap_item['RealPrice'],
            'discount': soap_item['Discount'],
            'amount': soap_item['Amount'],
            'shipped': soap_item['Shipped'],
            'unshipped': soap_item['Unshipped'],
            'in_stock': soap_item['InStock'],
            'awaiting': soap_item['Awaiting'],
            'awaiting_date': soap_item['AwaitingDate'],
            'unit': soap_item['Unit']
        }
        shipped_count += soap_item['Shipped']
        in_stock_count += soap_item['InStock']
        quantity_count += soap_item['Quantity']
        awaiting_count += soap_item['Awaiting']
        try:
            item = delete_idx.pop(soap_item['Article'])
            for key, value in update_fields.items():
                setattr(item, key, value)
            update_items.append(item)
        except KeyError:
            update_fields.update({'order': instance})
            created_items.append(ItemOrder(**update_fields))
    try:
        ItemOrder.objects.bulk_update(
            update_items,
            [i for i in update_fields.keys() if i != 'order']
        )
    except NameError:  # not update_fields not update_items
        pass
    ItemOrder.objects.bulk_create(created_items)
    ItemOrder.objects.filter(
        id__in=[i.id for i in delete_idx.values()]
    ).delete()
    instance.set_status(
        instance.Statuses.Data(
            shipped_count=shipped_count,
            in_stock_count=in_stock_count,
            quantity_count=quantity_count,
            awaiting_count=awaiting_count,
            closed=res['Closed'],
            approve=res['ApprovedByManager']
        )
    )
    instance.save()
