import django_filters
from django import forms

from apps.orders.models import Order
from apps.partner.models import Partner
from apps.users.models import DisplayUser


class OrderFilter(django_filters.FilterSet):
    status = django_filters.ChoiceFilter(
        choices=tuple(x for x in Order.STATUS[1:]),
        empty_label='Статус (все)',
        widget=forms.Select(attrs={
            'class': "form-control filter",
            'onchange': 'submit()',
            'title': 'Выберите статус'
        })
    )
    responsible = django_filters.ModelChoiceFilter(
        empty_label='Ответственный (все)',
        queryset=lambda request: DisplayUser.objects.filter(
            partners=Partner.objects.get(id=request.session['company'])
        ).exclude(is_staff=True),
        widget=forms.Select(attrs={
            'class': "form-control filter",
            'onchange': 'submit()',
            'title': 'Выберите ответственного'
        })
    )

    class Meta:
        model = Order
        fields = ['status', 'responsible']
