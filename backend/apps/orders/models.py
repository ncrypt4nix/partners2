import pyexcel_xlsx
from tempfile import NamedTemporaryFile
from django.db import models, transaction
from django.urls import reverse
from django.contrib.auth.models import User

from apps.catalog.models import Product
from apps.trackemail.models import InternalMessageInterface
from apps.utils.models import LastSync
from core.soap_interface import SoapInterface
from apps.orders.exceptions import (
    WrongFileFormat,
    RequiredProductDoesNotExis,
    EmptyFileError,
    FileErrorList
)
from apps.orders.tasks import send_email_change_status_order
from apps.helpdesk.models import Channel, TypeChannel


class Order(SoapInterface, InternalMessageInterface, LastSync):
    class Statuses:
        class Data:
            """Data required to define Order status"""
            needed_field = {
                'shipped_count': float,
                'in_stock_count': float,
                'quantity_count': float,
                'awaiting_count': float,
                'closed': bool,
                'approve': bool,
            }

            def __init__(self, **kwargs):
                for field, type_is in self.needed_field.items():
                    if (field not in kwargs
                            or not isinstance(kwargs[field], type_is)):
                        raise ValueError(
                            f'Field {field} needed to create Statuses.Data'
                        )
                    setattr(self, field, kwargs[field])

        TRASH = '0'
        DRAFT = '1'
        SEND = '2'
        PROCESSING = '3'
        READY_TO = '4'
        READY_TO_PARTIALLY = '5'
        RESERVE = '6'
        RESERVE_PARTIALLY = '7'
        SHIPPED = '8'
        SHIPPED_PARTIALLY = '9'
        CLOSED = '10'
        CANCELED = '11'
        LOCAL_STATUSES = [
            TRASH,
            DRAFT,
            SEND,
            CANCELED
        ]
        MATH_WITH_APPROVED = {
            PROCESSING: READY_TO,
            RESERVE: READY_TO,
            RESERVE_PARTIALLY: READY_TO_PARTIALLY,
            SHIPPED: SHIPPED,
            SHIPPED_PARTIALLY: SHIPPED_PARTIALLY,
            CLOSED: CLOSED,
        }
    STATUS = (
        (Statuses.TRASH, 'В корзине'),
        (Statuses.DRAFT, 'В черновике'),
        (Statuses.SEND, 'Запрос отправлен'),
        (Statuses.PROCESSING, 'В обработке'),
        (Statuses.READY_TO, 'Готов к отгрузке'),
        (Statuses.READY_TO_PARTIALLY, 'Готов к отгрузке (частично)'),
        (Statuses.RESERVE, 'В резерве'),
        (Statuses.RESERVE_PARTIALLY, 'В резерве (частично)'),
        (Statuses.SHIPPED, 'Отгружен'),
        (Statuses.SHIPPED_PARTIALLY, 'Отгружен (частично)'),
        (Statuses.CLOSED, 'Закрыт'),
        (Statuses.CANCELED, 'Отменен'),
    )
    partner = models.ForeignKey(
        'partner.Partner',
        related_name='orders',
        on_delete=models.CASCADE
    )
    status = models.CharField('Статус', max_length=2, choices=STATUS)
    comments = models.TextField('Комментарий к заказу', blank=True, null=True)
    order_id = models.CharField('ID', max_length=100, blank=True, null=True)
    total = models.FloatField('Сумма', blank=True, null=True)
    currency = models.CharField('Валюта', max_length=20, blank=True, null=True)
    created_1c = models.DateField('Создано в 1С', blank=True, null=True)
    number = models.CharField('Номер', max_length=20, blank=True, null=True)
    warehouse = models.TextField('Склад', blank=True, null=True)
    responsible = models.ForeignKey(
        User,
        verbose_name='Ответственный',
        related_name="orders",
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    ITEM_SERIALIZE_FIELDS = [
        'art',
        'quantity',
        'shipped',
        'in_stock',
        'awaiting',
        'real_price',
        'amount'
    ]
    EXCEL_SHEET_NAME = 'Order Sheet'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def repeat_order(self, responsible):
        """create and return new copy order with order items"""
        order = self._meta.model(
            status=self.Statuses.DRAFT,
            partner=self.partner,
            warehouse=self.warehouse,
            responsible=responsible,
        )
        order.save()
        items = []
        for item in self.items.all():
            product = item.product
            if not product:
                try:
                    product = Product.objects.get(art=item.art)
                except Product.DoesNotExist:
                    data = self.convert_articul_to_nomenclature(item.art)
                    product = Product(
                        art=data['Article'],
                        code_1c=data['Code'],
                        title=data['Name'],
                    )
                    product.save()
            items.append(
                ItemOrder(
                    order=order,
                    product=product,
                    art=item.art,
                    title=item.title,
                    quantity=item.quantity,
                )
            )
        ItemOrder.objects.bulk_create(items)
        return order

    def set_status(self, data):
        """update Order status without save
            :return bool status changed
        """
        if not isinstance(data, Order.Statuses.Data):
            raise ValueError('Wrong type: data')
        old_status = self.status
        if data.closed:
            self.status = Order.Statuses.CLOSED
        elif data.quantity_count == data.shipped_count:
            self.status = Order.Statuses.SHIPPED
        elif (data.quantity_count - data.shipped_count
                == data.in_stock_count + data.awaiting_count):
            self.status = Order.Statuses.RESERVE
        elif 0 < data.in_stock_count + data.awaiting_count:
            self.status = Order.Statuses.RESERVE_PARTIALLY
        elif 0 < data.shipped_count:
            self.status = Order.Statuses.SHIPPED_PARTIALLY
        else:
            self.status = Order.Statuses.PROCESSING
        if data.approve:
            self.status = Order.Statuses.MATH_WITH_APPROVED[self.status]
        if old_status != self.status:
            send_email_change_status_order.delay(self.id)
            return True
        return False

    @classmethod
    def get_serialize_fields(cls):
        """get map verbose_name: ItemOrder field to serialize"""
        fields_map = ItemOrder._meta._forward_fields_map
        return {
            field: fields_map[field].verbose_name
            for field in cls.ITEM_SERIALIZE_FIELDS
        }

    def to_excel(self):
        """generate excel file and return object in memory"""
        fields_map = ItemOrder._meta._forward_fields_map
        data = {
            self.EXCEL_SHEET_NAME: [
                [
                    fields_map[field].verbose_name
                    for field in self.ITEM_SERIALIZE_FIELDS
                ]
            ]
        }
        for order_item in self.items.all():
            data[self.EXCEL_SHEET_NAME].append(
                [
                    getattr(order_item, field)
                    for field in self.ITEM_SERIALIZE_FIELDS
                ]
            )
        file = NamedTemporaryFile(mode='w+b')
        file.name = file.name + '.xlsx'
        pyexcel_xlsx.save_data(file.name, data)
        return file

    @classmethod
    @transaction.atomic
    def from_excel(cls, file, partner):
        """ Create order and related order items from xlsx file
            may a raise exceptions:
                - WrongFileFormat
                - RequiredProductDoesNotExis
        """
        REQUIRED_LEN = 2
        # Fields
        ART = 0
        QUANTITY = 1

        def check_first_str_on_headers(data):
            fields_map = ItemOrder._meta._forward_fields_map
            headers = [
                fields_map[field].verbose_name
                for field in self.ITEM_SERIALIZE_FIELDS
            ]
            for num, item in enumerate(data[0]):
                if item != headers[num]:
                    return False
            return True
        self = cls(
            status=Order.Statuses.DRAFT,
            partner=partner,
            comments="From xlsx"
        )
        errors = []
        try:
            prepare_data = pyexcel_xlsx.get_data(file)
            data = prepare_data[next(iter(prepare_data))]
            if check_first_str_on_headers(data):
                data[0] = []
        except KeyError:
            raise EmptyFileError
        products = {
            i.art: i
            for i in Product.objects.filter(
                art__in=[
                    i[ART]
                    for i in data
                    if len(i) and isinstance(i[ART], str)
                ]
            )
        }
        for num, item in enumerate(data, 1):
            if not len(item):
                continue
            if (len(item) < REQUIRED_LEN or not
                    isinstance(item[ART], str) or not
                    ((isinstance(item[QUANTITY], int)) or
                        isinstance(item[QUANTITY], float)) or
                    item[QUANTITY] < 0):
                errors.append(WrongFileFormat(num))
            elif item[ART] not in products:
                errors.append(RequiredProductDoesNotExis(num))
        if errors:
            raise FileErrorList(errors)
        self.save()
        order_items = {}
        for item in data:
            if not len(item):
                continue
            if order_items.get(item[ART]):
                order_items[item[ART]].quantity += item[QUANTITY]
                continue
            order_items[item[ART]] = ItemOrder(
                order=self,
                product=products[item[ART]],
                quantity=item[QUANTITY],
                art=item[ART]
            )
        ItemOrder.objects.bulk_create(order_items.values())
        return self

    def create_disput(self, starter):
        """return Channel disput about self-order"""
        if self.status in self.Statuses.LOCAL_STATUSES:
            return Channel.get_support(self.partner)
        channel_type, created = TypeChannel.objects.get_or_create(
            ident=TypeChannel.ORDER_DISPUT,
            defaults={
                'title': 'Вопрос по заказу'
            }
        )
        if created:
            channel_type.supervisors.set(
                User.objects.filter(is_superuser=True)
            )
        channel, created = Channel.objects.get_or_create(
            reverse_pk=self.id,
            type=channel_type,
            defaults={
                'partner': self.partner
            }
        )
        return channel

    def get_absolute_url(self):
        return reverse('orders:detail', args=[self.pk])

    def __str__(self):
        return f'{self.number} {self.get_status_display()}'
