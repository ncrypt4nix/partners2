from django.views.generic.edit import FormView
from django.urls import reverse_lazy
from apps.feedback.forms import FeedBackForm
from apps.partner.views import PartnerViewMixin


class FeedBackView(PartnerViewMixin, FormView):
    template_name = 'feedback/feedback_form.html'
    form_class = FeedBackForm
    success_url = reverse_lazy('feedback:thanks')

    def form_valid(self, form):
        form.save(self.request, self.partner)
        return super().form_valid(form)
