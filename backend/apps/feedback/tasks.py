from core.celery import app
from apps.feedback.models import FeedBack


@app.task()
def send_feedback_message(pk):
    try:
        FeedBack.objects.get(pk=pk).send_modeled_email('feedback_message')
    except FeedBack.DoesNotExist:
        pass
