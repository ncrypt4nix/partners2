from django.db import models
from django.contrib.auth.models import User
from apps.trackemail.models import InternalMessageInterface
from apps.partner.models import Partner


class FeedBack(InternalMessageInterface, models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True
    )
    partner = models.ForeignKey(
        Partner,
        on_delete=models.SET_NULL,
        null=True
    )
    topic = models.CharField(max_length=128)
    message = models.TextField()

    def send_modeled_email(
        self,
        tpl_name=None,
        **kwargs
    ):
        tpl = self.get_email_message(tpl_ident=tpl_name)
        to = [
            x.email
            for x in self.partner.get_managers()
        ]
        self.send_full_email(to, tpl, **kwargs)
