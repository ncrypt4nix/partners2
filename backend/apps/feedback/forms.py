from django import forms
from apps.feedback.models import FeedBack
from apps.feedback.tasks import send_feedback_message


class FeedBackForm(forms.ModelForm):
    class Meta:
        model = FeedBack
        fields = ('topic', 'message')

    def save(self, request, partner):
        kwargs = {
            field: request.POST[field] for field in self.fields
        }
        feedback = self._meta.model(
            user=request.user,
            partner=partner,
            **kwargs
        )
        feedback.save()
        send_feedback_message.delay(feedback.pk)
