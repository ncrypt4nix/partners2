from django.apps import AppConfig


class FeedBackConfig(AppConfig):
    name = 'apps.feedback'
