from django.urls import path
from django.views.generic.base import TemplateView
from apps.feedback.views import FeedBackView


urlpatterns = [
    path('feedback/', FeedBackView.as_view(), name='feedback'),
    path(
        'thanks/',
        TemplateView.as_view(
            template_name='feedback/thanks_for_feedback.html'
        ),
        name='thanks'
    )

]
