from django import forms
from apps.catalog.models import Warehouse
from apps.partner.models import Partner
from apps.utils.logger import write_log


class PartnerWarehouses(forms.ModelForm):
    """Form add or remove warhouses to partner"""
    warehouses = forms.ModelMultipleChoiceField(
        queryset=Warehouse.objects.filter(visible=True),
        widget=forms.CheckboxSelectMultiple,
        label=""
    )

    class Meta:
        model = Partner
        fields = ['warehouses']

    def save(self, user):
        value = [i.name for i in self.cleaned_data['warehouses']]
        old_value = [i.name for i in self.instance.warehouses.all()]
        write_log(
            user,
            self.instance,
            'User change warhouses: from {from_value} to {to_value}'.format(
                from_value=old_value,
                to_value=value
            )
        )
        super().save()
