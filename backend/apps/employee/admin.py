from django.contrib import admin
from apps.employee.models import Branch


@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    fields = ('title', 'email', 'address', 'url', 'employees')
    list_display = ('title', )
