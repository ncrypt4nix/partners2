from django.test import TestCase
from core.utils import test_partner_view
from apps.partner.models import Partner
from django.db.models import Q


@test_partner_view
class TestEmployeePartnerListView(TestCase):
    fixtures = [
        'apps/helpdesk/fixtures/helpdesk_message.json',
        'apps/orders/fixtures/orders_order.json'
    ]

    test_url = 'employee:partners'

    def test_get_queryset(self):
        response = self.client.get(self.url)
        object_list = response.context['object_list']
        user = self.partner.manager
        user_partners = Partner.objects.filter(
            Q(users__in=[user]) or
            Q(branch__in=user.employee.branch_set.all())
        ).count()
        self.assertNotEqual(len(object_list), 0)
        self.assertEqual(sorted(object_list), list(object_list))
        self.assertEqual(len(object_list), user_partners)
        partners = []
        for item in object_list:
            self.assertNotIn(item, partners)
            partners.append(partners)
