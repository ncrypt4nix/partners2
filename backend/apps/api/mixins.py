import uuid
from apps.utils.logger import write_traffic_log
from apps.partner.models import Partner


class PartnerAPIMixin:
    def dispatch(self, request, *args, **kwargs):
        path = super().dispatch(request, *args, **kwargs)
        try:
            partner_code = self.request.META.get('Partner-code')
            partner = Partner.objects.get(api_code=uuid.UUID(hex=partner_code))
        except (Partner.DoesNotExist, TypeError):
            partner = None
        write_traffic_log(
            request.user,
            partner,
            self.request.get_full_path()
        )
        return path
