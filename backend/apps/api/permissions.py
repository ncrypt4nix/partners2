from rest_framework.permissions import IsAuthenticated


class IsPartnerAuthenticated(IsAuthenticated):
    """
    Allows access only to authenticated partners.
    """
    def has_permission(self, request, view):
        if request.auth:
            return int(request.auth.get('access_level', 0)) >= 25
        return False
