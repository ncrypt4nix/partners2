from django.urls import path, include

api_patterns = [
    path('v1/', include('apps.api.v1.urls'), name='v1'),
]

urlpatterns = [
    path('api/', include(api_patterns)),
]
