from rest_framework import serializers
from apps.catalog.models import Vendor


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = ('title', 'ident')
