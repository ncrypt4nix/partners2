from rest_framework.decorators import action
from rest_framework import viewsets

from apps.api.permissions import IsPartnerAuthenticated
from apps.api.mixins import PartnerAPIMixin
from apps.api.v1.pagination import ResultAndMaxPagePaginator
from apps.api.v1.products.serializers import ProductBaseSerializer
from apps.api.v1.vendors.serializers import VendorSerializer
from apps.catalog.models import Vendor


class ListRetrieveVendorView(PartnerAPIMixin, viewsets.ReadOnlyModelViewSet):
    pagination_class = ResultAndMaxPagePaginator
    permission_classes = (IsPartnerAuthenticated,)
    queryset = Vendor.objects.filter(published=True)
    serializer_class = VendorSerializer
    lookup_field = 'ident'

    @action(methods=['get'], detail=True)
    def products(self, request, *args, **kwargs):
        queryset = self.get_object().product_set.filter(
            published=True
        )
        serializer = ProductBaseSerializer(
            self.paginate_queryset(queryset),
            many=True,
            context=self.get_renderer_context()
        )
        return self.get_paginated_response(serializer.data)
