from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.api.v1.vendors.views import ListRetrieveVendorView
from apps.api.v1.products.views import ListRetrieveProductView


router = DefaultRouter()
router.register(r'vendors', ListRetrieveVendorView)
router.register(r'products', ListRetrieveProductView)

urlpatterns = [
    path('', include(router.urls)),
]
