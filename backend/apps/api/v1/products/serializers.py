import uuid
import pandas as pd

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from apps.catalog.models import Product, ProductPrice, WarehouseItem
from apps.partner.models import Partner


class ProductBaseSerializer(serializers.ModelSerializer):
    stock = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ('title', 'art', 'price', 'stock')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.partner = self.get_partner()
        if not self.partner:
            return
        products = args[0] if isinstance(args[0], list) else [args[0]]
        self.eu_prices = {
            price['product_id']: price['value']
            for price in ProductPrice.objects.filter(
                product__in=products,
                type='eu'
            ).values('product_id', 'value')
        }
        pd_keys = [
            'warehouse_name__name',
            'available_in_stock',
            'available_in_transit',
            'product'
        ]
        try:
            self.warehouses = pd.DataFrame(
                WarehouseItem.objects.filter(
                    product__in=products,
                    warehouse_name__in=self.partner.get_all_warehouses()
                ).values(*pd_keys)
            ).set_index('product')
        except KeyError:
            self.warehouses = pd.DataFrame(
                columns=pd_keys).set_index('product')

    def get_partner(self):
        partner_code = self.context['request'].headers.get('Partner-code', '')
        if not partner_code:
            return None
        try:
            return Partner.objects.get(api_code=uuid.UUID(hex=partner_code))
        except (ObjectDoesNotExist, ValueError):
            return None

    def get_price(self, obj):
        if not self.partner:
            return {'partner': None, 'EU': None}
        return {
            'partner': obj.get_partner_price(self.partner)[0],
            'EU': self.eu_prices.get(obj.id)
        }

    def get_stock(self, obj):
        if not self.partner:
            return []
        try:
            warehouses = self.warehouses.loc[obj.id]
        except KeyError:
            return []
        try:
            return {
                warehouse['warehouse_name__name']: {
                    'warehouse': warehouse['available_in_stock'],
                    'transit': warehouse['available_in_transit']
                }
                for num, warehouse in warehouses.iterrows()
            }
        except AttributeError:
            return {
                warehouses['warehouse_name__name']: {
                    'warehouse': warehouses['available_in_stock'],
                    'transit': warehouses['available_in_transit']
                }
            }


class ProductSerializer(ProductBaseSerializer):
    vendor = serializers.SlugRelatedField(read_only=True, slug_field='ident')

    class Meta(ProductBaseSerializer.Meta):
        fields = ProductBaseSerializer.Meta.fields + ('vendor',)
