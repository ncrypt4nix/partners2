from rest_framework import viewsets
from apps.api.permissions import IsPartnerAuthenticated
from apps.api.mixins import PartnerAPIMixin
from apps.api.v1.products.serializers import ProductSerializer
from apps.api.v1.pagination import ResultAndMaxPagePaginator
from apps.catalog.models import Product


class ListRetrieveProductView(PartnerAPIMixin, viewsets.ReadOnlyModelViewSet):
    pagination_class = ResultAndMaxPagePaginator
    permission_classes = (IsPartnerAuthenticated,)
    queryset = Product.objects.filter(published=True).select_related('vendor')
    serializer_class = ProductSerializer
    lookup_field = 'art'
