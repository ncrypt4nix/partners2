import requests
from django.conf import settings
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import ValidationError


class CASTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        user = requests.get(
            '{}/jwt_login/'.format(settings.CAS_SERVER_URL),
            headers={'Authorization': 'Token {}'.format(key)},
            verify=False
        ).json()
        if 'error' in user:
            raise ValidationError({'error': user['error']})
        return (None, user)
