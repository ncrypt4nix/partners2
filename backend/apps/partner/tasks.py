from apps.catalog.tasks import sync_partner_catalog
from apps.contracts.tasks import (
    sync_contracts,
    sync_rebates,
    sync_aggreements,
    sync_document_files,
    sync_partner_documents,
    sync_partner_orders,
)
from apps.partner.models import PartnerContactPersons, Partner
from core.celery import app


@app.task()
def sync_partners():
    for partner in Partner.objects.all():
        create_partner_sync_from_1c_task.delay(
            partner_code=partner.contragentcod,
        )


@app.task()
def create_partner_sync_from_1c_task(partner_code):
    instance = Partner.objects.get(contragentcod=partner_code)
    data = instance.get_info()
    if data.get('Error', {}).get('Flag'):
        instance.log = data['Error']['Description']
        instance.save()
        return
    partner_sync(instance, data)
    partner_contact_persons_sync(instance, data)
    sync_contracts(instance)
    sync_aggreements(instance)
    sync_rebates(instance)
    sync_document_files(instance)
    certification_partner_sync(instance)
    sync_partner_catalog(instance.id)
    sync_partner_documents(instance.id)
    sync_partner_orders(instance.id)


def partner_contact_persons_sync(instance, data):
    contacts = {
        '{surname}{name}{middlename}'.format(
            surname=i.surname,
            name=i.name,
            middlename=i.middlename
            ): i
        for i in instance.contact_persons.all()
    }
    create_contacts = list()
    update_contacts = list()
    for contact in data.get('ContactPersons'):
        phone = ''
        email = ''
        for contact_info in contact['ContactInformation']:
            if contact_info['Type'] == 'Телефон':
                phone = contact_info['Presentation']
            elif contact_info['Type'] == 'E-Mail':
                email = contact_info['Presentation']
        defaults = {
            'primary': contact['Primary'],
            'name': contact['Name'],
            'middlename': contact['MiddleName'],
            'surname': contact['Surname'],
            'occupation': contact['Position'],
            'phone': phone,
            'email': email
        }
        full_name = '{surname}{name}{middlename}'.format(
            surname=contact['Surname'],
            name=contact['Name'],
            middlename=contact['MiddleName']
        )
        try:
            el = contacts.pop(full_name)
            update_flug = False
            for key, value in defaults.items():
                if getattr(el, key) == value:
                    continue
                setattr(el, key, value)
                update_flug = True
            if update_flug:
                update_contacts.append(el)
        except KeyError:
            create_contacts.append(
                PartnerContactPersons(partner=instance, **defaults)
            )
    PartnerContactPersons.objects.bulk_create(create_contacts)
    try:
        PartnerContactPersons.objects.bulk_update(update_contacts, [*defaults])
    except NameError:  # do not items
        pass
    PartnerContactPersons.objects.filter(
        id__in=[i.id for i in contacts.values()]
    ).delete()
