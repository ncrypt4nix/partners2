from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.partner.models import Partner
from apps.partner.tasks import create_partner_sync_from_1c_task


@receiver(post_save, sender=Partner)
def create_partner_sync_from_1c(instance, created, **kwargs):
    if not created:
        return
    # countdown before real save partner
    create_partner_sync_from_1c_task.apply_async(
        [instance.contragentcod],
        countdown=6
    )
