from django import forms


class EmailForManagerForm(forms.Form):
    message = forms.CharField()
