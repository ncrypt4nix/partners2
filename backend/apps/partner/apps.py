from django.apps import AppConfig


class PartnerConfig(AppConfig):
    name = 'apps.partner'

    def ready(self):
        from . import signals
