from django.contrib import admin
from apps.partner.models import Partner
from apps.partner.tasks import create_partner_sync_from_1c_task
from mptt.admin import MPTTModelAdmin


@admin.register(Partner)
class PartnerAdmin(MPTTModelAdmin):
    change_form_template = 'admin/partner/partner_change_form.html'
    fields = (
        'users',
        'contragentcod',
        'manager',
        'branch',
        'parent',
        'log',
        'api_code'
    )
    readonly_fields = ('log', 'api_code')
    list_display = ('short_name', 'contragentcod', 'log')

    actions = ['update_contragent']

    def update_contragent(self, request, queryset):
        """Force update"""
        for partner in queryset:
            create_partner_sync_from_1c_task.delay(partner.contragentcod)
    update_contragent.short_description = "Force update contragent"
