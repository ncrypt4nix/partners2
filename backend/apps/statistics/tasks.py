import hashlib
import datetime
import math
from apps.catalog.models import Vendor
from apps.partner.models import Partner
from apps.contracts.models import Aggreement
from apps.statistics.models import SalesTransaction, AgreementsAccomplishment
from core.celery import app
from core.soap_interface import SoapInterface, ConnectionError


@app.task()
def sync_statistics(partner_code):
    def hash_key(number, art, date):
        return ''.join([
            hashlib.md5(number.encode()).hexdigest(),
            hashlib.md5(art.encode()).hexdigest(),
            hashlib.md5(str(date).encode()).hexdigest()
        ])

    soap = SoapInterface()
    vendors = {i.ident: i for i in Vendor.objects.all()}
    partner = Partner.objects.get(contragentcod=partner_code)

    try:
        date_from = SalesTransaction.objects.filter(
            partner=partner
        ).latest('date').date
    except SalesTransaction.DoesNotExist:
        date_from = datetime.date(
            year=datetime.date.today().year - 1,
            month=1,
            day=1
        )
    date_to = datetime.date.today()
    sales = {
        hash_key(i.number, i.art, i.date): i
        for i in SalesTransaction.objects.filter(
            partner=partner,
            date__gte=date_from,
            date__lte=date_to,
        )
    }
    half_year = 364 / 2
    periods = [
        (
            date_from + datetime.timedelta(days=half_year * i),
            date_from + datetime.timedelta(days=half_year * (i + 1)),
        )
        for i in range(math.ceil((date_to - date_from).days / half_year))
    ]
    data = list()
    for period in periods:
        try:
            soap_data = soap.get_statistics(
                date_from=period[0],
                date_to=period[1],
                partners=[partner.contragentcod]
            )
        except ConnectionError:
            return
        if soap_data['Error']['Flag']:
            return
        data += soap_data['SalesTable']
    update_sales = list()
    create_sales = list()
    for item in data:
        vendor = vendors.get(item['Brand'].lower())
        if not vendor:
            vendor = Vendor.objects.create(ident=item['Brand'].lower())
            vendors.update({vendor.ident: vendor})
        defaults = {
            'number': item['Number'],
            'partner': partner,
            'vendor': vendor,
            'date': item['Date'],
            'art': item['Article'],
            'desc': item['Description'],
            'quantity': item['Quantity'],
            'amount': item['Sum'],
            'unit': item['Unit'],
            'currency': item['Currency']
        }
        try:
            sale = sales.pop(
                hash_key(
                    item['Number'],
                    item['Article'],
                    item['Date']
                )
            )
            update_flug = False
            for key, value in defaults.items():
                if getattr(sale, key) == value:
                    continue
                setattr(sale, key, value)
                update_flug = True
            if update_flug:
                update_sales.append(sale)
        except KeyError:
            create_sales.append(SalesTransaction(**defaults))
    SalesTransaction.objects.bulk_create(create_sales)
    try:
        SalesTransaction.objects.bulk_update(
            update_sales, [*defaults.keys()]
        )
    except NameError:  # not update_fields not update_items
        pass
    SalesTransaction.objects.filter(
        id__in=[i.id for i in sales.values()]
    ).delete()


@app.task()
def sync_partners_agreements_accomplishment_history():
    def accomplishments_hash_key(aggrement, date_from):
        date_from_hash = hashlib.md5(str(date_from).encode())
        aggrement_hash = (
            hashlib.md5(str(aggrement).encode())
            if not isinstance(aggrement, hashlib._hashlib.HASH)
            else aggrement
        )
        return f'{aggrement_hash.hexdigest()}{date_from_hash.hexdigest()}'

    partners = {i.contragentcod: i for i in Partner.objects.all()}
    date_from = Aggreement.objects.earliest('date').date
    try:
        data = SoapInterface().get_partners_agreements_accomplishment_history(
            [*partners.keys()],
            date_from=date_from,
            date_to=datetime.date.today()
        )
    except ConnectionError:
        return
    if data['Error']['Flag']:
        return
    agreements = {
        i.id_1c: i
        for i in Aggreement.objects.filter(partner__in=partners.values())
    }
    accomplishments = {
        accomplishments_hash_key(i.aggrement.id_1c, i.date_from): i
        for i in AgreementsAccomplishment.objects.all()
    }
    update_accomplishments = list()
    create_accomplishments = list()
    for partner in data['Partners']:
        for agreement_soap in partner['Agreements']:
            if agreement_soap['ID'] not in agreements:
                continue
            agreement = agreement_soap['ID']
            agreement_hash = hashlib.md5(str(agreement).encode())
            for period in agreement_soap['Periods']:
                defaults = {
                    'aggrement': agreements[agreement],
                    'current_sum': period['Sum'],
                    'date_from': period['DateFrom'],
                }
                try:
                    accomplishment = accomplishments.pop(
                        accomplishments_hash_key(
                            agreement_hash,
                            period['DateFrom']
                        )
                    )
                    update_flug = False
                    for key, value in defaults.items():
                        if getattr(accomplishment, key) == value:
                            continue
                        setattr(accomplishment, key, value)
                        update_flug = True
                    if update_flug:
                        update_accomplishments.append(accomplishment)
                except KeyError:
                    create_accomplishments.append(
                        AgreementsAccomplishment(**defaults)
                    )

    AgreementsAccomplishment.objects.bulk_create(create_accomplishments)
    AgreementsAccomplishment.objects.bulk_update(
        update_accomplishments,
        [*defaults.keys()]
    )
    AgreementsAccomplishment.objects.filter(
        id__in=[i.id for i in accomplishments.values()]
    ).delete()
