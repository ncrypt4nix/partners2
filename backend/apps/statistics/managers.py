from django.db import models
from django.db. models import Q, When, Case, Value
from core.soap_interface import SoapInterface, ConnectionError


class AgreementsAccomplishmentManager(models.Manager):
    def get_last_soap_agreements(self, partner):
        soap = SoapInterface()
        data = soap.get_partner_agreements_accomplishment(
            partner.contragentcod
        )
        if data['Error']['Flag']:
            raise ConnectionError

        qs = self.get_queryset()

        if not data['Agreements']:
            return qs.none()

        aggreements = {
            i.id_1c: i
            for i in partner.partner_aggreements.all()
            if i.id_1c
        }
        q = Q()
        case_expr = []
        model_list = []
        for item in data['Agreements']:
            aggrement = aggreements.get(item['ID'])
            if not aggrement:
                continue
            defaults = {
                'aggrement': aggrement,
                'date_from': item['DateFrom']
            }
            q |= Q(**defaults)
            case_expr.append(When(**defaults, then=Value(item['Sum'])))
            model_list.append(self.model(**defaults, current_sum=item['Sum']))

        qs = qs.filter(q)
        if case_expr:
            qs.update(current_sum=Case(*case_expr))
        if qs.count() == len(model_list):
            return qs

        qs_to_compare = [(i.aggrement, i.date_from) for i in qs]
        qs.model.objects.bulk_create([
            i
            for i in model_list
            if (i.aggrement, i.date_from) not in qs_to_compare
        ])
        return self.get_queryset().filter(q)
