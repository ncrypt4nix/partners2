import decimal
import datetime
from django.test import TestCase
from django.db.models.query import QuerySet
from core.utils import test_partner_view
from apps.contracts.fields import PeriodTypeField
from apps.partner.models import Partner
from apps.statistics.models import AgreementsAccomplishment, SalesTransaction
from apps.statistics.templatetags import statistics_tags
from apps.statistics.tasks import (
    sync_partners_agreements_accomplishment_history,
    sync_statistics
)


class TestTasks(TestCase):
    """test celery tasks about statistics"""
    fixtures = [
        'apps/partner/fixtures/partner.json',
        'apps/contracts/fixtures/aggreement.json',
    ]

    def test_sync_partners_agreements_accomplishment_history(self):
        """test sync agreements accomplishment history from soap"""
        partner = Partner.objects.last()
        qs = AgreementsAccomplishment.objects.filter(
            aggrement__in=partner.partner_aggreements.all()
        )
        self.assertEqual(qs.count(), 0)
        sync_partners_agreements_accomplishment_history()
        self.assertNotEqual(qs.count(), 0)

    def test_sync_statistics(self):
        """test celery task get statistics from soap"""
        partner = Partner.objects.last()
        qs = SalesTransaction.objects.filter(partner=partner)
        self.assertEqual(qs.count(), 0)
        sync_statistics(partner_code=partner.contragentcod)
        self.assertNotEqual(qs.count(), 0)


class TestAgreementsAccomplishmentModel(TestCase):
    fixtures = [
        'apps/partner/fixtures/partner.json',
        'apps/contracts/fixtures/aggreement.json',
    ]

    def test_sync_last_agreements(self):
        TEST_SUM = 0.93
        partner = Partner.objects.last()
        TEST_DEFAULTS = {
            'aggrement': partner.partner_aggreements.first(),
            'date_from': datetime.date.today() - datetime.timedelta(days=180)
        }
        AgreementsAccomplishment(
            current_sum=TEST_SUM,
            **TEST_DEFAULTS
        ).save()
        func = AgreementsAccomplishment.objects.get_last_soap_agreements
        agreements = func(partner)
        self.assertIsInstance(agreements, QuerySet)
        self.assertNotEqual(agreements.count(), 0)
        # test on old values not changed
        self.assertEqual(
            AgreementsAccomplishment.objects.get(**TEST_DEFAULTS).current_sum,
            decimal.Decimal(str(TEST_SUM))
        )

    def test_period_number(self):
        partner = Partner.objects.last()
        aggrement = partner.partner_aggreements.first()
        aa = AgreementsAccomplishment(
            aggrement=aggrement,
            current_sum=0,
            date_from=datetime.date(2015, 11, 5)
        )
        PeriodType = PeriodTypeField.PeriodType
        self.assertIsInstance(aa.aggrement.period_type, PeriodType)
        if aa.aggrement.period_type == PeriodType.QUARTER:
            self.assertEqual(aa.period_number, 4)
        elif aa.aggrement.period_type == PeriodType.HALF_YEAR:
            self.assertEqual(aa.period_number, 2)
        else:
            raise self.AssertionError()


@test_partner_view
class TestStatisticsView(TestCase):
    test_url = 'statistics:statistics'

    def test_first_sync(self):
        response = self.client.get(self.url)
        self.assertNotEqual(len(response.context['object_list']), 0)
        for item in response.context['object_list']:
            self.assertEqual(item.partner, self.partner)
        self.assertIsNotNone(self.client.session['statistics_last_sync'])
        first_sync = self.client.session['statistics_last_sync']
        # test on not sync for delta
        response = self.client.get(self.url)
        self.assertEqual(
            first_sync,
            self.client.session['statistics_last_sync']
        )


@test_partner_view
class TestAgreementsAccomplishmentView(TestCase):
    fixtures = [
        'apps/contracts/fixtures/aggreement.json',
    ]
    test_url = 'statistics:agreements_accomplishment'

    def test_get_queryset(self):
        response = self.client.get(self.url)
        self.assertNotEqual(len(response.context['object_list']), 0)
        aggrements = []
        for item in response.context['object_list']:
            self.assertEqual(item.aggrement.partner, self.partner)
            self.assertNotIn(item.aggrement, aggrements)
            aggrements.append(item.aggrement)


@test_partner_view
class TestAgreementsAccomplishmentHistoryView(TestCase):
    fixtures = [
        'apps/contracts/fixtures/aggreement.json',
        'apps/statistics/fixtures/agreements_accomplishment.json',
    ]
    test_url = 'statistics:agreements_accomplishment_history'

    def test_get_queryset(self):
        response = self.client.get(self.url)
        sync_partners_agreements_accomplishment_history()
        self.assertNotEqual(len(response.context['object_list']), 0)
        current_quarter = (
            AgreementsAccomplishment.objects.get_last_soap_agreements(
                self.partner
            )
        )
        for item in response.context['object_list']:
            self.assertEqual(item.aggrement.partner, self.partner)
            self.assertNotIn(item, current_quarter)


class TestTemplateTags(TestCase):
    def test_percentage_of(self):
        self.assertEqual(
            statistics_tags.percentage_of(
                decimal.Decimal(162382),
                decimal.Decimal(300000)
            ).quantize(decimal.Decimal('0.2')),
            decimal.Decimal(54.12).quantize(decimal.Decimal('0.2'))
        )
        self.assertEqual(
            statistics_tags.percentage_of(
                decimal.Decimal(300),
                decimal.Decimal(100)
            ),
            decimal.Decimal(300)
        )
        self.assertEqual(
            statistics_tags.percentage_of(
                decimal.Decimal(0),
                decimal.Decimal(1)
            ),
            decimal.Decimal(0)
        )

    def test_remainder_of(self):
        self.assertEqual(
            statistics_tags.remainder_of(
                decimal.Decimal(53.5),
                decimal.Decimal(80)
            ),
            decimal.Decimal(26.5)
        )
        self.assertEqual(
            statistics_tags.remainder_of(
                decimal.Decimal(81),
                decimal.Decimal(80)
            ),
            decimal.Decimal(-1)
        )

    def test_reverse_sign(self):
        self.assertEqual(statistics_tags.reverse_sign(str(10)), '10')
        self.assertEqual(statistics_tags.reverse_sign(str(-10)), '+10')
