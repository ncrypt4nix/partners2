from django.db import models
from apps.contracts.models import Aggreement
from apps.statistics.managers import AgreementsAccomplishmentManager


class AgreementsAccomplishment(models.Model):
    """Accomplishment for Partner Agreements"""
    aggrement = models.ForeignKey(Aggreement, on_delete=models.CASCADE)
    current_sum = models.DecimalField(max_digits=10, decimal_places=2)
    date_from = models.DateField()
    objects = AgreementsAccomplishmentManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['aggrement', 'date_from'],
                name='unique_agreements_accomplishment'
            )
        ]
        indexes = [
            models.Index(fields=['date_from']),
        ]
        ordering = ['-date_from', '-aggrement__product_type_presentation']

    @property
    def period_number(self):
        return self.aggrement.period_type.period_number(self.date_from)
