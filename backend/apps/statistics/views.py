import functools
import datetime
from django.db.models import Q, Max
from django.core.exceptions import FieldError
from django.views.generic import ListView
from django_filters.views import FilterView
from django.db.models import Sum, Min
from apps.partner.views import PartnerViewMixin
from apps.statistics.filters import SalesFilter, AgreementsAccomplishmentFilter
from apps.statistics.models import SalesTransaction, AgreementsAccomplishment
from apps.statistics.tasks import sync_statistics


def statistics_synced(method):
    """sync statistics before run class method"""
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        LAST_SYNC = 'statistics_last_sync'
        delta = 5 * 60
        now = datetime.datetime.now().timestamp()
        if (not self.request.session.get(LAST_SYNC) or
                delta < now - self.request.session[LAST_SYNC]):
            sync_statistics(partner_code=self.partner.contragentcod)
            self.request.session[LAST_SYNC] = now
        return method(self, *args, **kwargs)
    return wrapper


class StatisticsView(PartnerViewMixin, FilterView):
    template_name = 'statistics/statistics.html'
    paginate_by = 25
    filterset_class = SalesFilter

    @statistics_synced
    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        try:
            kwargs['date_from'] = SalesTransaction.objects.filter(
                partner=self.partner
            ).earliest('date').date
        except SalesTransaction.DoesNotExist:
            kwargs['date_from'] = SalesTransaction.objects.filter(
                partner=self.partner
            ).aggregate(Min('date')).get('date__min')
            if not kwargs['date_from']:
                kwargs['date_from'] = datetime.date.today()
        return kwargs

    @statistics_synced
    def get_queryset(self):
        qs = self.partner.statistics.all()
        if 'sort' in self.request.GET:
            try:
                sorted_qs = qs.order_by(
                    self.request.GET['sort']
                )
                # execute queryset to verify exist field for order_by
                sorted_qs.first()
                return sorted_qs
            except FieldError:
                pass
        return qs

    def get_context_data(self, **kwargs):
        querystring = self.request.GET.copy()
        querystring.pop('page', None)
        cnt = super().get_context_data(**kwargs)
        cnt['sum'] = self.filterset.qs.aggregate(Sum('amount'))['amount__sum']
        cnt['querystring'] = querystring.urlencode()
        return cnt


class AgreementsAccomplishmentView(PartnerViewMixin, ListView):
    """view agreements accomplishment for this period a current partner"""
    template_name = 'statistics/agreements_accomplishment.html'

    def get_queryset(self):
        return AgreementsAccomplishment.objects.get_last_soap_agreements(
            self.partner
        ).select_related('aggrement').order_by(
            '-date_from', '-aggrement__product_type_presentation'
        )


class AgreementsAccomplishmentViewHistoryView(PartnerViewMixin, FilterView):
    """view agreements accomplishment history for current partner"""
    template_name = 'statistics/agreements_accomplishment_history.html'
    paginate_by = 25
    filterset_class = AgreementsAccomplishmentFilter

    def get_queryset(self):
        qs = AgreementsAccomplishment.objects.filter(
            aggrement__partner=self.partner
        ).select_related('aggrement')
        agreements = self.partner.partner_aggreements.annotate(
            latest_date=Max('agreementsaccomplishment__date_from')
        )
        q = Q()
        for agreement in agreements:
            q |= Q(aggrement=agreement, date_from=agreement.latest_date)
        return qs.exclude(q).order_by(
            '-date_from', '-aggrement__product_type_presentation'
        )
