import django_filters
from apps.contracts.models import Aggreement
from apps.partner.models import Partner
from apps.statistics.models import AgreementsAccomplishment


class AgreementsAccomplishmentFilter(django_filters.FilterSet):
    aggrement = django_filters.ModelChoiceFilter(
        empty_label='Все документы',
        queryset=lambda request: Aggreement.objects.filter(
            partner=Partner.objects.get(id=request.session['company'])
        )
    )

    class Meta:
        model = AgreementsAccomplishment
        fields = ('aggrement',)
