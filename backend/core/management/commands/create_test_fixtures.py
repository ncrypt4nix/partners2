from django.core import management
from django.core.management.base import BaseCommand
from django.core.management.commands import dumpdata
from apps.catalog.models import Product


class Command(BaseCommand):
    help = 'create all needed fixtures for test'
    products = Product.objects.all()[:10]
    apps = {
        'apps/partner/fixtures/partner.json': (
            {},
            (
                'partner.Partner',
                'employee.Branch',
                'catalog.Warehouse',
                'auth.User',
            ),
        ),
        'apps/contracts/fixtures/aggreement.json': (
            {},
            (
                'contracts.Aggreement',
                'contracts.Contract',
            ),
        ),
        'apps/users/fixtures/users.json': (
            {},
            (
                'users.Attributes',
                'auth.User',
            ),
        ),
        'apps/statistics/fixtures/agreements_accomplishment.json': (
            {},
            (
                'statistics.AgreementsAccomplishment',
                'contracts.Aggreement',
            ),
        ),
        'apps/catalog/fixtures/catalog_products.json': (
            {'pks': ','.join([str(i.id) for i in products])},
            (
                'catalog.Product',
            ),
        ),
        'apps/catalog/fixtures/catalog_sections.json': (
            {'pks': ','.join([
                ','.join(
                    [
                        str(j.id)
                        for j in i.section.get_ancestors(include_self=True)
                    ]
                )
                for i in products
            ])},
            (
                'catalog.Section',
            ),
        ),
        'apps/catalog/fixtures/catalog_vendors.json': (
            {},
            (
                'catalog.Vendor',
            ),
        ),
        'apps/orders/fixtures/orders_order.json': (
            {},
            (
                'orders.Order',
                'partner.Partner',
                'auth.User',
            ),
        ),
        'apps/helpdesk/fixtures/helpdesk_message.json': (
            {},
            (
                'helpdesk.Message',
                'helpdesk.Channel',
                'helpdesk.TypeChannel',
                'auth.User',
            ),
        )
    }

    def handle(self, *args, **options):
        for file, params in self.apps.items():
            print(f'dump to file: {file}')
            management.call_command(
                dumpdata.Command(),
                natural_foreign=True,
                natural_primary=True,
                traceback=True,
                output=file,
                *params[1],
                **params[0],
            )
        self.stdout.write(self.style.SUCCESS('dumpdata is done!'))
