from django.db.models import Q
from apps.partner.models import Partner
from apps.helpdesk.models import Channel, Message


def only_authenticated(func):
    def wrapper(*args, **kwargs):
        try:
            request = args[0]
        except IndexError:
            return {}
        if not request.user.is_authenticated:
            return {}
        return func(*args, **kwargs)
    return wrapper


@only_authenticated
def partners(request):
    try:
        partner = Partner.objects.get(
            id=request.session.get('company')
        )
        holding_qs = partner.get_family().filter(users=request.user)
        cnt = {
            'is_holding': holding_qs.count() > 1,
            'holding_partners': holding_qs
        }
    except Partner.DoesNotExist:
        return {}
    cnt.update(count_cart(request, partner))
    cnt.update(catalog_vendors())
    cnt.update(unread_messages(request))
    cnt.update(sending_orders(request, partner))
    cnt.update(error_sync_soap_time())
    cnt.update(need_webpush_ask(request))
    return cnt


def count_cart(request, partner):
    objects = request.user.attributes.cart(partner).order_items()
    cart = list(objects.values_list('product', flat=True))
    count = objects.count()
    return {'cart_count': count, 'cart': cart}


@only_authenticated
def unread_messages(request):
    return {
        'unread_messages': Message.objects.filter(
                channel__in=Channel.user_channels(request.user)
            ).exclude(
                Q(read__in=[request.user]) |
                Q(sender=request.user)
            ).count()
    }
