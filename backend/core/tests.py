import datetime
from django.test import TestCase
from core.soap_interface import SoapInterface
from apps.partner.models import Partner
from apps.catalog.models import Product
from apps.contracts.models import Aggreement


class TestSoapInterface(TestCase):
    """testing api to soap interface"""
    fixtures = [
        'apps/partner/fixtures/partner.json',
        'apps/contracts/fixtures/aggreement.json',
        'apps/catalog/fixtures/catalog_vendors.json',
        'apps/catalog/fixtures/catalog_sections.json',
        'apps/catalog/fixtures/catalog_products.json',
    ]

    def setUp(self):
        self.soap = SoapInterface()

    def test_get_partner_agreements_on_change(self):
        """test soap api function GetPartnerAgreements on change"""
        partner = Partner.objects.last()
        data = self.soap.get_partner_agreements(partner.contragentcod)
        self.assertFalse(data['Error']['Flag'])
        self.assertIsInstance(data['Agreements'], list)
        item = data['Agreements'][0]
        self.assertIsInstance(item, dict)
        self.assertIsInstance(item['ID'], int)
        self.assertIsInstance(item['PartnerCode'], str)
        self.assertIsInstance(item['Date'], datetime.date)
        self.assertIsInstance(item['AggreementTypeId'], str)
        self.assertIsInstance(item['AggreementTypePresentation'], str)
        self.assertIsInstance(item['ProductAggreementTypeId'], str)
        self.assertIsInstance(item['ProductAggreementTypePresentation'], str)
        self.assertIsInstance(item['Status'], str)
        self.assertIsInstance(item['StatusPresentation'], str)
        self.assertIsInstance(item['QuarterAmount'], float)
        self.assertIsInstance(item['ContractDate'], datetime.date)
        self.assertIsInstance(item['ContractNumber'], str)
        self.assertIsInstance(item['PeriodType'], str)

    def test_get_agreement_info_on_change(self):
        """test soap api function GetAgreementInfo on change"""
        aggreement = Aggreement.objects.last()
        data = self.soap.get_agreement_info(aggreement.id_1c)
        self.assertFalse(data['Error']['Flag'])
        item = data['Agreement']
        self.assertIsInstance(item['ID'], int)
        self.assertIsInstance(item['PartnerCode'], str)
        self.assertIsInstance(item['Date'], datetime.date)
        self.assertIsInstance(item['AggreementTypeId'], str)
        self.assertIsInstance(item['AggreementTypePresentation'], str)
        self.assertIsInstance(item['ProductAggreementTypeId'], str)
        self.assertIsInstance(item['ProductAggreementTypePresentation'], str)
        self.assertIsInstance(item['Status'], str)
        self.assertIsInstance(item['StatusPresentation'], str)
        self.assertIsInstance(item['QuarterAmount'], float)
        self.assertIsInstance(item['ContractDate'], datetime.date)
        self.assertIsInstance(item['ContractNumber'], str)
        self.assertIsInstance(item['PeriodType'], str)

    def test_get_partner_agreements_accomplishment(self):
        """test soap api function GetPartnerAgreementsAccomplishment"""
        partner = Partner.objects.last()
        data = self.soap.get_partner_agreements_accomplishment(
            partner.contragentcod
        )
        self.assertFalse(data['Error']['Flag'])
        self.assertIsInstance(data['Agreements'], list)
        item = data['Agreements'][0]
        self.assertIsInstance(item['ID'], int)
        self.assertIsInstance(item['Sum'], float)
        self.assertIsInstance(item['DateFrom'], datetime.date)

    def test_get_partners_agreements_accomplishment_history(self):
        """test soap api function GetPartnersAgreementsAccomplishmentHistory"""
        partner = Partner.objects.last()
        date_to = datetime.datetime.now()
        date_from = date_to - datetime.timedelta(days=30 * 2)
        data = self.soap.get_partners_agreements_accomplishment_history(
            [partner.contragentcod],
            date_from=date_from,
            date_to=date_to
        )
        self.assertFalse(data['Error']['Flag'])
        self.assertIsInstance(data['Partners'], list)
        partners = data['Partners'][0]
        self.assertEqual(partners['PartnerCode'], partner.contragentcod)
        self.assertIsInstance(partners['Agreements'], list)
        aggreement = partners['Agreements'][0]
        self.assertIsInstance(aggreement['ID'], int)
        self.assertIsInstance(aggreement['Periods'], list)
        if not aggreement['Periods']:
            return
        period = aggreement['Periods'][0]
        self.assertIsInstance(period['Sum'], float)
        self.assertIsInstance(period['DateFrom'], datetime.date)

    def test_convert_articul_to_nomenclature(self):
        """test soap api function FindByArticle"""
        art = Product.objects.first().art
        data = self.soap.convert_articul_to_nomenclature(art)
        self.assertIsInstance(data['Nomenclatures'], list)
        item = data['Nomenclatures'][0]
        self.assertIsInstance(item['Code'], str)
        self.assertIsInstance(item['Article'], str)
        self.assertIsInstance(item['Name'], str)

    def test_get_installers(self):
        data = self.soap.get_installers()
        self.assertIsInstance(data['Installers'], list)
        installer = data['Installers'][0]
        self.assertIsInstance(installer['TorgCode'], str)
        self.assertIsInstance(installer['Name'], str)
        self.assertIsInstance(installer['Town'], str)
        self.assertIsInstance(installer['ActualAddress'], str)
        self.assertIsInstance(installer['Phone'], str)
        self.assertIsInstance(installer['Site'], str)
        self.assertIsInstance(installer['Certificates'], list)
        self.assertIsInstance(installer['Specialists'], list)
        if not installer['Certificates']:
            return
        certificate = installer['Certificates'][0]
        self.assertIsInstance(certificate['Name'], str)
        self.assertIsInstance(certificate['DateOfIssue'], datetime.date)
        self.assertIsInstance(certificate['ExpiryDate'], datetime.date)
        if not installer['Specialists']:
            return
        specialist = installer['Specialists'][0]
        self.assertIsInstance(specialist['FullName'], str)
        self.assertIsInstance(specialist['Certificates'], list)
        if not specialist['Certificates']:
            return
        certificate_specialist = specialist['Certificates'][0]
        self.assertIsInstance(certificate_specialist['Name'], str)
        self.assertIsInstance(
            certificate_specialist['DateOfIssue'],
            datetime.date
        )
        self.assertIsInstance(
            certificate_specialist['ExpiryDate'],
            datetime.date
        )
